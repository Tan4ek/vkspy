package com.ssr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.sheduling.ReportLastSeenJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.ArrayList;
import java.util.List;


//@EnableAdminServer
@SpringBootApplication// same as @Configuration @EnableAutoConfiguration @ComponentScan
//@EnableWebMvc отлитает загрузка .js и всего статического,http://stackoverflow.com/questions/24661289/spring-boot-not-serving-static-content
public class Application extends WebMvcConfigurerAdapter {

    @Autowired
    private ObjectMapper objectMapper;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    @Bean
    @Primary
    public Validator getValidator() {

        LocalValidatorFactoryBean localValidatorFactoryBean = new LocalValidatorFactoryBean();
        return localValidatorFactoryBean;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/static/");
    }


    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        final String vkSpyView = "vkSpyHome";
        registry.addViewController("/").setViewName(vkSpyView);
        registry.addViewController("/login").setViewName(vkSpyView);
        registry.addViewController("/vk").setViewName(vkSpyView);
        registry.addViewController("/profile").setViewName(vkSpyView);
        registry.addViewController("/vk/profile").setViewName(vkSpyView);
    }

    @Override
    public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
        List<HttpMessageConverter<?>> meessagesConverters = new ArrayList<>();
        meessagesConverters.add(new MappingJackson2HttpMessageConverter(objectMapper));
    }


    public static class JobWorker {
        public final boolean skipJob;

        public JobWorker(boolean skipJob) {
            this.skipJob = skipJob;
        }
    }

    @Bean(name = "booleanWraper")
    public JobWorker getWraper() {
        return new JobWorker(false);
    }

    @Autowired
    private ReportLastSeenJob reportLastSeenJob;

    @Autowired
    private SpyInfo2Repository spyInfo2Repository;

//    @Autowired
//    private ObjectMapper objectMapper;

    @Value("${notification.mail.mapping.salt:auauauitebiavseravnonaidu}")
    private String saltSha1;

    @Bean
    public CommandLineRunner demo() {
        return (args) -> {

//            ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
//
//            ClassLoader classLoader = getClass().getClassLoader();
//            File file = new File(classLoader.getResource("static/js/nashorn.js").getFile());
//            List<String> strings = Files.readAllLines(file.toPath());
//            System.out.println(strings);
//
//            String nashornJs = strings.stream().collect(Collectors.joining("\n"));
//            ScriptContext context = engine.getContext();
//            context.setAttribute("spyInfoRepository", spyInfo2Repository, ScriptContext.ENGINE_SCOPE);
//
//            context.setAttribute("azaza", "azaza11", ScriptContext.ENGINE_SCOPE);
//
//            engine.eval(nashornJs);
//            Object eval = engine.eval("azaza");
//            System.out.println(eval);
//
//            int portNumber = 45417;
//
//            try (
//                    ServerSocket serverSocket = new ServerSocket(portNumber);
//                    Socket clientSocket = serverSocket.accept();
//                    PrintWriter out =
//                            new PrintWriter(clientSocket.getOutputStream(), true);
//                    BufferedReader in = new BufferedReader(
//                            new InputStreamReader(clientSocket.getInputStream()));
//            ) {
//
//                String inputLine, outputLine;
//
//                // Initiate conversation with client
//                KnockKnockProtocol kkp = new KnockKnockProtocol();
//                outputLine = kkp.processInput(null);
//                out.println(outputLine);
//
//                while ((inputLine = in.readLine()) != null) {
//                    String jsonResult;
//                    try {
//                        jsonResult = objectMapper.writeValueAsString(engine.eval(inputLine));
//                    } catch (Exception e) {
//                        System.out.println(e);
//                        jsonResult = "error " + e.getLocalizedMessage();
//                    }
//                    out.println(jsonResult);
////                    outputLine = kkp.processInput(inputLine);
////                    out.println(outputLine);
//                    if (jsonResult.equals("exit"))
//                        break;
//                }
//            } catch (IOException e) {
//                System.out.println("Exception caught when trying to listen on port "
//                        + portNumber + " or listening for a connection");
//                System.out.println(e.getMessage());
//            }


//            List<SpyInfo> oldSpyInfos = spyInfoRepository.findAll();
//
//            List<SpyInfo2> newSpyInfos = oldSpyInfos.stream().map(oldSpyInfo -> {
//                SpyInfo2 spyInfo2 = new SpyInfo2();
//                List<NotificationEntity> notificationEntities = oldSpyInfo.getNotificationMails().stream()
//                        .map(mail -> new NotificationEntity(HashGenerator.generateNotificationHash(mail, oldSpyInfo.getVkId()), mail, true, true))
//                        .collect(Collectors.toList());
//                spyInfo2.setVkId(oldSpyInfo.getVkId());
//                spyInfo2.setNotificationEntities(notificationEntities);
//                spyInfo2.setVkUser(oldSpyInfo.getVkUser());
//                return spyInfo2;
//            }).collect(Collectors.toList());
//
//            spyInfo2Repository.save(newSpyInfos);

//            for (SpyInfo2 newSpyInfo : newSpyInfos) {
//                if (!spyInfo2Repository.exists(newSpyInfo.getVkId())) {
//                    spyInfo2Repository.save(newSpyInfo);
//                }
//            }

//            List<SpyInfo> all = spyInfoRepository.findAll();
//
//            List<NotificationMailMapping> notificationMailMappings = new ArrayList<>(all.size());
//            for (SpyInfo spyInfo : all) {
//                String vkId = spyInfo.getVkId();
//
//                List<String> notificationMails = spyInfo.getNotificationMails();
//
//                for (String notificationMail : notificationMails) {
//                    String id = DigestUtils.sha1Hex(saltSha1 + notificationMail + vkId);
//                    NotificationMailMapping existMapping = notificationMailMappingRepository.findOneByNotificationMailAndVkId(notificationMail, vkId);
//                    if (existMapping == null) {
//                        notificationMailMappings.add(new NotificationMailMapping(id, vkId, notificationMail));
//                    }
//                }
//            }
//
//            notificationMailMappingRepository.save(notificationMailMappings);
        };
    }

    public static class KnockKnockProtocol {
        private static final int WAITING = 0;
        private static final int SENTKNOCKKNOCK = 1;
        private static final int SENTCLUE = 2;
        private static final int ANOTHER = 3;

        private static final int NUMJOKES = 5;

        private int state = WAITING;
        private int currentJoke = 0;

        private String[] clues = {"Turnip", "Little Old Lady", "Atch", "Who", "Who"};
        private String[] answers = {"Turnip the heat, it's cold in here!",
                "I didn't know you could yodel!",
                "Bless you!",
                "Is there an owl in here?",
                "Is there an echo in here?"};

        public String processInput(String theInput) {
            String theOutput = null;

            if (state == WAITING) {
                theOutput = "Knock! Knock!";
                state = SENTKNOCKKNOCK;
            } else if (state == SENTKNOCKKNOCK) {
                if (theInput.equalsIgnoreCase("Who's there?")) {
                    theOutput = clues[currentJoke];
                    state = SENTCLUE;
                } else {
                    theOutput = "You're supposed to say \"Who's there?\"! " +
                            "Try again. Knock! Knock!";
                }
            } else if (state == SENTCLUE) {
                if (theInput.equalsIgnoreCase(clues[currentJoke] + " who?")) {
                    theOutput = answers[currentJoke] + " Want another? (y/n)";
                    state = ANOTHER;
                } else {
                    theOutput = "You're supposed to say \"" +
                            clues[currentJoke] +
                            " who?\"" +
                            "! Try again. Knock! Knock!";
                    state = SENTKNOCKKNOCK;
                }
            } else if (state == ANOTHER) {
                if (theInput.equalsIgnoreCase("y")) {
                    theOutput = "Knock! Knock!";
                    if (currentJoke == (NUMJOKES - 1))
                        currentJoke = 0;
                    else
                        currentJoke++;
                    state = SENTKNOCKKNOCK;
                } else {
                    theOutput = "Bye.";
                    state = WAITING;
                }
            }
            return theOutput;
        }
    }

    //	@Bean(name ="templateEngine")
//	public SpringTemplateEngine templateEngine() {
//		SpringTemplateEngine engine = new SpringTemplateEngine();
//		engine.setTemplateResolver(getTemplateResolver());
//		return engine;
//	}
//
//	@Bean(name="viewResolver")
//	public ThymeleafViewResolver getViewResolver(){
//		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
//		viewResolver.setTemplateEngine(templateEngine());
//		return viewResolver;
//	}
//
//	@Bean(name ="templateResolver")
//	public ServletContextTemplateResolver getTemplateResolver() {
//		ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver();
//		templateResolver.setPrefix("/resources/templates/");
//		templateResolver.setSuffix(".html");
//		templateResolver.setTemplateMode("XHTML");
//		return templateResolver;
//	}

//	@Bean
//	public LocaleResolver localeResolver() {
//		SessionLocaleResolver slr = new SessionLocaleResolver();
//		slr.setDefaultLocale(Locale.US);
//		return slr;
//	}


//	@Bean(name ="messageSource")
//	public MessageSource getMessageSource() {
//		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//		messageSource.setBasename("/resource/messages/");
//		messageSource.setDefaultEncoding("UTF-8");
//		return messageSource;
//	}
}
