package com.ssr;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.ssr.entitys.Id;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.ServiceClientCredentialsFlowResponse;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Properties;

/**
 * Created by ssr on 18.01.17.
 */
@Configuration
// есть сценарий с установкой WebMvcConfigureSupport, Но это == @EnableWebMvc, из-за чего загрузка статики идет поездой (не понимаю как править)
public class BeansConfigurator extends WebMvcConfigurerAdapter {

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addSerializer(Id.class, new Id.IdSerializer());
        module.addDeserializer(Id.class, new Id.IdDeserializer());
        objectMapper.registerModule(module);

        return objectMapper;
    }

    @Bean
    public VkApiClient getVkAppClient() {
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vkApiClient = new VkApiClient(transportClient);
        return vkApiClient;
    }

    @Bean
    public ServiceActor getServiceActor(@Autowired VkApiClient vkApiClient, @Value("${vk.secret.key}") String secretKey, @Value("${vk.app.id}") Integer appId) throws ClientException, ApiException {
        ServiceClientCredentialsFlowResponse authResponse = vkApiClient.oauth()
                .serviceClientCredentialsFlow(appId, secretKey)
                .execute();

        ServiceActor actor = new ServiceActor(appId, secretKey, authResponse.getAccessToken());
        return actor;
    }

    @Bean(name = "methodInvokingDetailFactoryBean")
    public MethodInvokingJobDetailFactoryBean getMethodInvokingJobDetailFactoryBean() {
        MethodInvokingJobDetailFactoryBean invokingJobDetailFactoryBean = new MethodInvokingJobDetailFactoryBean();
        invokingJobDetailFactoryBean.setTargetBeanName("vkCheckFriends");
        invokingJobDetailFactoryBean.setTargetMethod("checkFriends");
        return invokingJobDetailFactoryBean;
    }

    @Bean(name = "methodInvokingCheckLastSeen")
    public MethodInvokingJobDetailFactoryBean getMethodInvokingLastSeenJob() {
        MethodInvokingJobDetailFactoryBean invokingJobDetailFactoryBean = new MethodInvokingJobDetailFactoryBean();
        invokingJobDetailFactoryBean.setTargetBeanName("lastSeenVkJob");
        invokingJobDetailFactoryBean.setTargetMethod("checkLastSeen");
        return invokingJobDetailFactoryBean;
    }

    @Bean(name = "methodInvokingReportLastSeen")
    public MethodInvokingJobDetailFactoryBean getMethodInvokingReportLastSeen() {
        MethodInvokingJobDetailFactoryBean methodInvokingJobDetailFactoryBean = new MethodInvokingJobDetailFactoryBean();
        methodInvokingJobDetailFactoryBean.setTargetBeanName("reportLastSeenJob");
        methodInvokingJobDetailFactoryBean.setTargetMethod("makeReport");
        methodInvokingJobDetailFactoryBean.setName("reportLastSeenName");
        return methodInvokingJobDetailFactoryBean;
    }

    @Bean(name = "checkFriendsTrigger")
    public SimpleTriggerFactoryBean trigger(@Autowired @Qualifier(value = "methodInvokingDetailFactoryBean") JobDetail job) {
        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
        trigger.setJobDetail(job);
        trigger.setStartDelay(120_000);
        trigger.setRepeatInterval(6_000_000);
        trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
        return trigger;
    }

    @Bean(name = "checkLastSeenTrigger")
    public SimpleTriggerFactoryBean checkLastSeenTrigger(@Autowired @Qualifier(value = "methodInvokingCheckLastSeen") JobDetail lastSeenJob) {
        SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
        trigger.setJobDetail(lastSeenJob);
        trigger.setRepeatInterval(200_000);
        trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);


        return trigger;
    }

    @Bean(name = "reportLastSeenTrigger")
    public Trigger reportLastSeen(@Autowired @Qualifier(value = "methodInvokingReportLastSeen") JobDetail reportLastSeen) {
        return TriggerBuilder.newTrigger()
                .withIdentity("reportLastSeenTrigger", "lastSeenGroup")
                .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(23, 30)) // fire every day at 23:00
                .forJob(reportLastSeen)
                .build();
    }

    @Bean
    public SchedulerFactoryBean scheduler(@Autowired @Qualifier(value = "checkFriendsTrigger") Trigger checkFriendsTrigger, @Autowired @Qualifier(value = "methodInvokingDetailFactoryBean") JobDetail checkFriendsJob,
                                          @Autowired @Qualifier(value = "checkLastSeenTrigger") Trigger checkLastSeenTrigger, @Autowired @Qualifier(value = "methodInvokingCheckLastSeen") JobDetail checkLastSeenJob,
                                          @Autowired @Qualifier(value = "reportLastSeenTrigger") Trigger reportLastSeenTrigger, @Autowired @Qualifier(value = "methodInvokingReportLastSeen") JobDetail reportLastSeenJobDetail) throws SchedulerException {
        System.out.println("scheduler");
        SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setJobFactory(springBeanJobFactory());
        schedulerFactory.setJobDetails(checkFriendsJob, checkLastSeenJob, reportLastSeenJobDetail);
        schedulerFactory.setTriggers(checkFriendsTrigger, checkLastSeenTrigger, reportLastSeenTrigger);
        schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));
        return schedulerFactory;
    }

    @Bean
    public SpringBeanJobFactory springBeanJobFactory() {
        SpringBeanJobFactory jobFactory = new SpringBeanJobFactory();
        return jobFactory;
    }

    @Value("${mail.properties.path}")
    private String mailPropertiesPath;

    @Bean
    public JavaMailSender mailSender() throws IOException {

        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Resource mailProperties = new FileSystemResource(mailPropertiesPath);
        if (!mailProperties.exists()) {
            mailProperties = new ClassPathResource("mail.properties");
            if (!mailProperties.exists()) {
                throw new IllegalStateException("Не удалось прочитать настройки smtp");
            }
        }

        Properties javaMailProperties = PropertiesLoaderUtils.loadProperties(mailProperties);
        mailSender.setJavaMailProperties(javaMailProperties);
        mailSender.setHost(javaMailProperties.getProperty("mail.host"));
        mailSender.setPort(Integer.parseInt(javaMailProperties.getProperty("mail.port")));
        mailSender.setUsername(javaMailProperties.getProperty("mail.username"));
        mailSender.setPassword(javaMailProperties.getProperty("mail.password"));
        return mailSender;
    }

    @Bean
    public ResourceBundleMessageSource emailMessageSource() {
        final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        return messageSource;
    }

    @Bean
    public TemplateEngine emailTemplateEngine() {
        final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
        // Resolver for TEXT emails
        templateEngine.addTemplateResolver(textTemplateResolver());
        // Resolver for HTML emails (except the editable one)
        templateEngine.addTemplateResolver(htmlTemplateResolver());
        // Message source, internationalization specific to emails
        templateEngine.setTemplateEngineMessageSource(emailMessageSource());
        return templateEngine;
    }

    private ITemplateResolver textTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setOrder(1);
        templateResolver.setResolvablePatterns(Collections.singleton("text/*"));
        templateResolver.setPrefix("/mail/");
        templateResolver.setSuffix(".txt");
        templateResolver.setTemplateMode("TEXT");
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

    private ITemplateResolver htmlTemplateResolver() {
        final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setOrder(2);
        templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
        templateResolver.setPrefix("/mail/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
        templateResolver.setCacheable(false);
        return templateResolver;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new CommonModelInterceptor());
    }
}
