package com.ssr.beans;

import com.ssr.entitys.db.NotificationEntity;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by ssr on 02.04.17.
 */
@Component
public class DomainConfiguration {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String currentIp;
    private final String rootUrl;

    public DomainConfiguration(@Value("${spring.config.ip:@null}") String currentIp) {
        this.currentIp = currentIp;
        if (currentIp != null) {
            rootUrl = "https://" + currentIp;
        } else {
            rootUrl = "";
        }
    }

    public String getRootUrl() {
        return rootUrl;
    }

    public boolean isExistDomain() {
        return currentIp != null;
    }

    public void fillNotificationProfile(Map<String, Object> modal, NotificationEntity notificationEntity) {
        if (isExistDomain()) {
            String hash = notificationEntity.getHash();
            if (StringUtils.isNoneEmpty(hash)) {
                logger.debug("fill notificationProfile for" + notificationEntity.getNotificationMail());
                modal.put("notificationProfile", getRootUrl() + "/vk/profile?profile=" + hash);
            }
        }
    }
}
