package com.ssr.beans;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by ssr on 25.03.17.
 */
public class HashGenerator {
    private static String HASH_SALT;

    @Value("${notification.mail.mapping.salt:auauauitebiavseravnonaidu}")
    public void setSalt(String salt) {
        if (HASH_SALT == null) {
            HASH_SALT = salt;
        }
    }

    public static String generateNotificationHash(String notificationMail, String vkId) {
        return DigestUtils.sha1Hex(HASH_SALT + notificationMail + vkId);
    }

    public static String getHashSalt() {
        return HASH_SALT;
    }
}
