package com.ssr.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by ssr on 11.10.16.
 */
@Controller
@RequestMapping(value = {"", "/"})
public class HomeController {

// TODO: 18.01.17 исправить, из-за этого не работает login
//    @RequestMapping(name = "registration", method = RequestMethod.GET)
//    public String registration() {
//        return "registration";
//    }
//
//    @RequestMapping(name = "registration", method = RequestMethod.POST)
//    public String registration(@Valid User user) {
//        user.setUserRole(UserRole.USER);
//        user.setName("testName");
//
//        userService.registrationUser(user);
//        return "redirect:/";
//    }


    @GetMapping("login")
    public String login() {
        return "login";
    }
//    @RequestMapping(value = "questionExam", method = RequestMethod.GET)
//    public String questionExam() {
//        return "questionExam";
//    }

}
