package com.ssr.controllers;

import com.ssr.beans.DomainConfiguration;
import com.ssr.entitys.DiffFriendsMailEntity;
import com.ssr.entitys.LastSeenTimestamp;
import com.ssr.entitys.LastSeenTimestampPeriod;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.entitys.db.SpyLastSeen;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.repository.SpyLastSeenRepository;
import com.ssr.services.MailService;
import com.ssr.services.UserService;
import com.ssr.utils.VkUtils;
import com.vk.api.sdk.objects.users.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by ssr on 22.03.17.
 */
@Controller
@RequestMapping("/test")
@Profile("test")
public class TestController {
    @Autowired
    private MailService mailService;

    @Autowired
    private SpyInfo2Repository spyInfoRepository;

    @Autowired
    private SpyLastSeenRepository spyLastSeenRepository;

    @GetMapping(value = "getMailDiff")
    public String diffMail(Model model) {
        List<SpyInfo2> all = spyInfoRepository.findAll();
        SpyInfo2 spyInfo = all.get(0);

        List<User> collect = all.stream().map((spyInfo1) -> (User) spyInfo1.getVkUser()).collect(Collectors.toList());
        DiffFriendsMailEntity user = new DiffFriendsMailEntity(spyInfo.getVkUser(), collect, collect, collect);
        model.addAttribute("user", user);
        if (domainConfiguration.isExistDomain()) {
            String hash = spyInfo.getNotificationEntities().get(0).getHash();
            model.addAttribute("callbackunsubscribe", domainConfiguration.getRootUrl() + "/vk/unsubscribe?id=" + hash);
        }
        return "mails/diffFriendsTemplate";
    }


    @Value(value = "${spring.config.ip:@null}")
    private String currentIp;

    @Autowired
    private DomainConfiguration domainConfiguration;

    @GetMapping(value = "lastSeenReport")
    public String unsubscribeMail(Model model) {
        List<SpyInfo2> spyInfos = Collections.singletonList(spyInfoRepository.findOne("402105273"));

        final ZoneId MOSCOW_ZONE = VkUtils.MOSCOW_ZONE;

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(MOSCOW_ZONE);
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);


        List<SpyLastSeen> spyLastSeens = spyLastSeenRepository.findBetweenDate(todayMidnight.atZone(MOSCOW_ZONE).toInstant(),
                tomorrowMidnight.atZone(MOSCOW_ZONE).toInstant(),
                spyInfos.stream().map(SpyInfo2::getVkId).collect(Collectors.toList()));

        Map<String, SpyInfo2> vkIdToSpyInfo = spyInfos.stream().collect(Collectors.toMap(SpyInfo2::getVkId, Function.identity()));

        mailService.sendEmail("ssr205@yandex.ru", "Стартовал отчет online", "Для : " +
                spyInfos.stream().flatMap(spyInfo1 -> spyInfo1.getNotificationEntities().stream().map(NotificationEntity::getNotificationMail)).collect(Collectors.joining(" ")));

        for (SpyLastSeen spyLastSeen : spyLastSeens) {
            List<LastSeenTimestamp> lastSeenTimestamps = spyLastSeen.getLastSeen().stream()
                    .map(LastSeenTimestamp::of)
                    .sorted(Comparator.comparing(LastSeenTimestamp::getTimestamp))
                    .collect(Collectors.toList());

            List<LastSeenTimestampPeriod> onlinePeriods = UserService.getLastSeenTimestampPeriods(lastSeenTimestamps);

            SpyInfo2 spyInfo = vkIdToSpyInfo.get(spyLastSeen.getVkId());
            for (NotificationEntity notificationEntity : spyInfo.getNotificationEntities()) {
                if (!notificationEntity.isSendOnlineReport()) {
                    continue;
                }
                Map<String, Object> modal = new HashMap<>();
                modal.put("today", todayMidnight.toLocalDate().format(DateTimeFormatter.ISO_DATE));
                modal.put("user", spyInfo.getVkUser());
                modal.put("onlinePeriods", onlinePeriods);
                if (domainConfiguration.isExistDomain()) {
                    String hash = notificationEntity.getHash();
                    modal.put("notificationProfile", domainConfiguration.getRootUrl() + "/vk/profile?profile=" + hash);
                }
                model.addAllAttributes(modal);
                ModelAndView modelAndView = new ModelAndView("mails/onlineReport", modal);
                String html = mailService.generate(modelAndView);
                mailService.sendHtmlEmail(notificationEntity.getNotificationMail(), "Online report", html);
                return "mails/onlineReport";
            }
        }
        return "";
    }
}
