package com.ssr.controllers;

import com.ssr.beans.DomainConfiguration;
import com.ssr.beans.HashGenerator;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.entitys.web.ProfileInfo;
import com.ssr.entitys.web.ProfileNotificationEntity;
import com.ssr.entitys.web.SaveProfileInfo;
import com.ssr.entitys.web.SubscribeVkSpy;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.services.MailService;
import com.ssr.services.SpyService;
import com.ssr.services.UserService;
import com.ssr.utils.VkUtils;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.validation.DirectFieldBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by ssr on 09.03.17.
 */
@Controller
@RequestMapping(value = {"/vk"})
public class VkFriendController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VkApiClient vkApiClient;

    @Autowired
    private MailService mailService;

    @Autowired
    private SpyInfo2Repository spyInfo2Repository;
    @Autowired
    private UserService userService;
    @Autowired
    private DomainConfiguration domainConfiguration;
    @Autowired
    private SpyService spyService;

    @Value("${notification.mail.mapping.salt:auauauitebiavseravnonaidu}")
    private String saltSha1;

    @RequestMapping(value = "saveFriendToCkeck", method = RequestMethod.POST, consumes = "application/x-www-form-urlencoded;charset=UTF-8", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String saveFriendToCheck(@Valid SubscribeVkSpy subscribeVkSpy) throws ClientException, ApiException, BindException {
        //TODO samohvalov: уйти от vkId
        List<UserXtrCounters> execute;
        try {
            execute = vkApiClient.users().get().userIds(subscribeVkSpy.getVkId()).fields(VkUtils.STANDART_USER_FIELDS).execute();
        } catch (ApiException e) {
            DirectFieldBindingResult incorrectVkId = new DirectFieldBindingResult(subscribeVkSpy, "subscribeVkSpy");
            incorrectVkId.addError(new FieldError("subscribeVkSpy", "vkId", e.getDescription()));
            throw new BindException(incorrectVkId);
        }

        if (CollectionUtils.isEmpty(execute)) {
            DirectFieldBindingResult notificationMail = new DirectFieldBindingResult(subscribeVkSpy, "subscribeVkSpy");
            notificationMail.addError(new FieldError("subscribeVkSpy", "vkId", "VK user not found"));
            throw new BindException(notificationMail);
        }
        UserXtrCounters vkUser = execute.get(0);
        String vkId = "" + vkUser.getId();

        boolean existsSpyInfoVkId = spyInfo2Repository.exists(vkId);
        NotificationEntity notificationEntity = null;
        if (existsSpyInfoVkId) {
            if (!spyInfo2Repository.existNotificationEmail(vkId, subscribeVkSpy.getNotificationMail())) {
                notificationEntity = getNotificationEntity(subscribeVkSpy);
                spyInfo2Repository.pushNotificationEntity(vkId, notificationEntity);
            } else {
                logger.warn("Попытка заново на пользователя " + vkId + " для email " + subscribeVkSpy.getNotificationMail());
                //TODO samohvalov: продумать что делать
            }
        } else {
            SpyInfo2 spyInfo = new SpyInfo2();
            spyInfo.setVkUser(vkUser);
            spyInfo.setVkId(vkId);
            notificationEntity = getNotificationEntity(subscribeVkSpy);
            spyInfo.setNotificationEntities(Collections.singletonList(notificationEntity));
            spyInfo2Repository.insert(spyInfo);
        }

        Map<String, Object> model = new HashMap<>();
        model.put("user", vkUser);
        ModelAndView modelAndView = new ModelAndView("mails/spySave", model);
        if (notificationEntity != null) {
            domainConfiguration.fillNotificationProfile(model, notificationEntity);
        }
        String html = mailService.generate(modelAndView);

        mailService.sendEmail("ssr205@yandex.ru", "Кто-то подписался на уведомления",
                "Для пользователя " + vkUser.getLastName() + " " + vkUser.getFirstName() + " " + vkUser.getId() + " для email " + subscribeVkSpy.getNotificationMail());

        mailService.sendHtmlEmail(subscribeVkSpy.getNotificationMail(), "Вы подписалиль на обновления!", html);

        return "redirect:/vk";
    }

    private NotificationEntity getNotificationEntity(@Valid SubscribeVkSpy subscribeVkSpy) {
        String hash = HashGenerator.generateNotificationHash(subscribeVkSpy.getNotificationMail(), subscribeVkSpy.getVkId());
        return new NotificationEntity(hash, subscribeVkSpy.getNotificationMail(), subscribeVkSpy.isSendOnlineReport(), subscribeVkSpy.isSendChangeFriendsReport());
    }

    @GetMapping(value = "/profile/{notificationHash}")
    @ResponseBody
    public ProfileInfo getProfile(@PathVariable("notificationHash") String notifHash) {
        //TODO samohvalov: придумать сущность для возвращаемых значений
        SpyInfo2 spyInfo = spyInfo2Repository.findByHash(notifHash);
        if (spyInfo == null || spyInfo.getNotificationEntities() == null || spyInfo.getNotificationEntities().size() == 0) {
            throw new IllegalArgumentException("notificationHash not present");
        }
        ProfileInfo profileInfo = new ProfileInfo();
        ProfileNotificationEntity profileNotificationEntity = new ProfileNotificationEntity(spyInfo.getNotificationEntities().get(0));
        profileNotificationEntity.setVkId(spyInfo.getVkId());
        profileNotificationEntity.setUser(spyInfo.getVkUser());
        profileInfo.setNotificationMail(spyInfo.getNotificationEntities().get(0).getNotificationMail());
        profileInfo.setProfileEntity(profileNotificationEntity);
        return profileInfo;
    }

    @PostMapping(value = "/profile/{notificationHash}/sendLastSeen", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity sendLastSeen(@PathVariable("notificationHash") String notifHash, @RequestBody Map<String, String> values) {
        String valueDate = values.get("date");
        LocalDate requestDate = LocalDate.parse(valueDate, DateTimeFormatter.ISO_DATE);
        LocalDate now = LocalDate.now(VkUtils.MOSCOW_ZONE);
        if (requestDate.isBefore(now) || now.isEqual(requestDate)) {
            SpyInfo2 spyInfo = spyInfo2Repository.findByHash(notifHash);
            if (spyInfo == null || spyInfo.getNotificationEntities() == null || spyInfo.getNotificationEntities().size() == 0) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
            LocalDateTime todayMidnight = LocalDateTime.of(requestDate, LocalTime.MIDNIGHT);
            userService.sendLastSeenEmails(Collections.singletonList(spyInfo), todayMidnight, todayMidnight.plusDays(1), notificationEntity -> Objects.equals(notificationEntity.getHash(), notifHash));
            return ResponseEntity.ok(null);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
    }

    @PostMapping(value = "/profile/save", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public void saveProfile(@RequestBody SaveProfileInfo saveProfileInfo) {
        SpyInfo2 spyInfo = spyInfo2Repository.findByHash(saveProfileInfo.getProfileId());
        if (spyInfo == null) {
            throw new IllegalArgumentException("Not valid profile");
        }
        HashSet<NotificationEntity> notificationEntities = new HashSet<>(spyInfo.getNotificationEntities());
        NotificationEntity targetNotEntity = notificationEntities.stream().findAny().filter(notificationEntity -> StringUtils.equals(notificationEntity.getHash(), saveProfileInfo.getProfileId())).orElse(null);
        if (targetNotEntity == null) {
            throw new IllegalArgumentException("Не удалось найти сущность из документа");
        }

        NotificationEntity updateEntities = new NotificationEntity(saveProfileInfo.getProfileId(), targetNotEntity.getNotificationMail(), saveProfileInfo.isSendOnlineReport(), saveProfileInfo.isSendChangeFriendsReport());
        notificationEntities.remove(updateEntities);
        notificationEntities.add(updateEntities);
        spyInfo.setNotificationEntities(new ArrayList<>(notificationEntities));
        spyInfo2Repository.save(spyInfo);
    }

    @GetMapping(value = "/profile/{notificationHash}/visitation/csv")
    @ResponseBody
    public void getVisitationCsv(@PathVariable("notificationHash") String notificationHash, HttpServletResponse response) {
        try {
            SpyInfo2 spyInfo = spyInfo2Repository.findByHash(notificationHash);
            response.setContentType("text/csv; charset=utf-8");
            response.setHeader("Content-Disposition","attachment;filename=visitation_id" + spyInfo.getVkId() + ".csv");
//            ServletOutputStream out = response.getOutputStream();
//            out.println(myString);
//            out.flush();
//            out.close();
            PrintWriter writer = response.getWriter();

            String csvVisitation = spyService.getCsvVisitation(spyInfo.getVkId());
            writer.write(csvVisitation);
            writer.close();
            writer.flush();

        } catch (IOException e) {
            throw new IllegalStateException("Не удалось записать данные в файл");
        }
    }

    @GetMapping(value = "unsubscribe")
    @ResponseBody
    public String unsubscribe(@RequestParam(name = "id") String id) {
        logger.debug("unsubscribe user with hash = " + id);

        SpyInfo2 byHash = spyInfo2Repository.findByHash(id);
        byHash.getNotificationEntities().stream().filter(notificationEntity -> StringUtils.equals(notificationEntity.getNotificationMail(), id)).findAny().ifPresent(notificationEntity -> {
            logger.debug("unsubscribe from " + byHash.getVkId() + " notificationEmail: " + notificationEntity.getNotificationMail());
        });

        boolean isUpdated = spyInfo2Repository.pullNotificationEntity(id);
        if (isUpdated) {
            return "OK";
        } else {
            return "NOT OK";
        }
    }

    @PostMapping(value = "unsubscribe", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public String unsubscribe(@RequestBody Map<String, String> values) {
        if (MapUtils.isEmpty(values) || !values.containsKey("profile")) {
            throw new IllegalArgumentException("Нет profile");
        }

        if (true) {
            return "OK";
        }

        String id = values.get("profile");

        logger.debug("unsubscribe user with hash = " + id);

        SpyInfo2 byHash = spyInfo2Repository.findByHash(id);
        byHash.getNotificationEntities().stream().filter(notificationEntity -> StringUtils.equals(notificationEntity.getNotificationMail(), id)).findAny().ifPresent(notificationEntity -> {
            logger.debug("unsubscribe from " + byHash.getVkId() + " notificationEmail: " + notificationEntity.getNotificationMail());
        });

        boolean isUpdated = spyInfo2Repository.pullNotificationEntity(id);
        if (isUpdated) {
            return "OK";
        } else {
            throw new IllegalStateException("Удаление не произошло");
        }
    }
}
