package com.ssr.entitys;

import java.util.List;

/**
 * Created by ssr on 08.04.17.
 */
public final class DiffFriendsMailEntity {
    public DiffFriendsMailEntity(com.vk.api.sdk.objects.users.User user, List<com.vk.api.sdk.objects.users.User> diffFriends, List<com.vk.api.sdk.objects.users.User> added, List<com.vk.api.sdk.objects.users.User> removed) {
        this.targetUser = user;
        this.diffFriends = diffFriends;
        this.added = added;
        this.removed = removed;
    }

    private final com.vk.api.sdk.objects.users.User targetUser;
    private final List<com.vk.api.sdk.objects.users.User> diffFriends;

    private final List<com.vk.api.sdk.objects.users.User> added;
    private final List<com.vk.api.sdk.objects.users.User> removed;

    public List<com.vk.api.sdk.objects.users.User> getAdded() {
        return added;
    }

    public List<com.vk.api.sdk.objects.users.User> getRemoved() {
        return removed;
    }

    public com.vk.api.sdk.objects.users.User getTargetUser() {
        return targetUser;
    }

    public List<com.vk.api.sdk.objects.users.User> getDiffFriends() {
        return diffFriends;
    }
}
