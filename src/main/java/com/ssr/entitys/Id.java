package com.ssr.entitys;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Created by ssr on 03.02.17.
 */
public class Id {
    private String id;

    private Id() {
    }

    public Id(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return id;
    }

    public static final Id NULLL = new Id();

    public static Id getInstance(String source) {
        return source == null ? NULLL : new Id(source);
    }

    public static final class IdSerializer extends StdSerializer<Id> {
        public IdSerializer() {
            super(Id.class);
        }


        @Override
        public void serialize(Id value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeString(value.getId());
        }
    }

    public static final class IdDeserializer extends StdDeserializer<Id> {

        public IdDeserializer() {
            super(Id.class);
        }

        @Override
        public Id deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
            return Id.getInstance(p.getValueAsString());
        }
    }
}
