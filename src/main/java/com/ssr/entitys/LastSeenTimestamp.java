package com.ssr.entitys;

import com.ssr.entitys.db.LastSeenEntity;

import java.time.Instant;
import java.util.Objects;

/**
 * Created by ssr on 08.04.17.
 */
public class LastSeenTimestamp {
    private final boolean isOnline;
    private final Instant lastSeenInstant;
    private final Instant timestamp;
    private final LastSeenEntity spyLastSeenEntity;


    public LastSeenTimestamp(LastSeenEntity spyLastSeen) {
        this.spyLastSeenEntity = spyLastSeen;
        this.isOnline = spyLastSeen.isOnline();
        this.timestamp = spyLastSeen.getTimestamp().toInstant();
        this.lastSeenInstant = Instant.ofEpochSecond(spyLastSeen.getLastSeen().getTime());
    }

    public LastSeenTimestamp(LastSeenEntity spyLastSeen, boolean isOnline, Instant timestamp) {
        this.spyLastSeenEntity = spyLastSeen;
        this.isOnline = isOnline;
        this.timestamp = timestamp;
        this.lastSeenInstant = Instant.ofEpochSecond(spyLastSeen.getLastSeen().getTime());
    }

    public LastSeenEntity getSpyLastSeenEntity() {
        return spyLastSeenEntity;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public Instant getLastSeenInstant() {
        return lastSeenInstant;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public static LastSeenTimestamp of(LastSeenEntity spyLastSeenEntity) {
        return new LastSeenTimestamp(spyLastSeenEntity);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LastSeenTimestamp that = (LastSeenTimestamp) o;
        return isOnline == that.isOnline &&
                Objects.equals(lastSeenInstant, that.lastSeenInstant) &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(spyLastSeenEntity, that.spyLastSeenEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isOnline, lastSeenInstant, timestamp, spyLastSeenEntity);
    }
}
