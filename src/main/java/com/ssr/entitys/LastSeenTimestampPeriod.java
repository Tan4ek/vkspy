package com.ssr.entitys;

import com.ssr.utils.VkUtils;
import org.springframework.util.Assert;

import java.time.format.DateTimeFormatter;

/**
 * Created by ssr on 08.04.17.
 */
public class LastSeenTimestampPeriod {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm").withZone(VkUtils.MOSCOW_ZONE);
    private final LastSeenTimestamp start;
    private final LastSeenTimestamp finish;

    public LastSeenTimestampPeriod(LastSeenTimestamp start, LastSeenTimestamp finish) {
        Assert.notNull(start);
        Assert.notNull(finish);
        this.start = start;
        this.finish = finish;
    }

    public LastSeenTimestampPeriod(LastSeenTimestamp single) {
        this(single, single);
    }

    public LastSeenTimestamp getStart() {
        return start;
    }

    public LastSeenTimestamp getFinish() {
        return finish;
    }

    public boolean isEmptyPeriod() {
        return start == finish;
    }

    @Override
    public String toString() {
        // TODO: 19.03.17 перенести на messageResource
        if (isEmptyPeriod()) {
            return DATE_TIME_FORMATTER.format(start.getTimestamp());
        }
        return "С " + DATE_TIME_FORMATTER.format(start.getTimestamp()) + " по " + DATE_TIME_FORMATTER.format(finish.getTimestamp());
    }
}
