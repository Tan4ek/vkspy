package com.ssr.entitys;

import com.ssr.utils.UserRole;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

/**
 * Created by ssr on 18.01.17.
 */
public class User implements Serializable {

    @Id
    private String login;
    private String name;
    private String email;
    private String password;

    private UserRole userRole;

    public User() {
    }

    public User(String name, String email, String login, String password, UserRole userRole) {
        this.name = name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.userRole = userRole;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return String.format("[login=%s, email=%s, userRole=%s]", login, email, userRole);
    }
}
