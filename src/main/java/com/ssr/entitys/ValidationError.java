package com.ssr.entitys;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ssr on 26.03.17.
 */
public class ValidationError {
    private List<ValidationErrorField> fieldErrors = new ArrayList<>();

    public void addFieldError(String path, String message) {
        ValidationErrorField error = new ValidationErrorField(path, message);
        fieldErrors.add(error);
    }

    public List<ValidationErrorField> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(List<ValidationErrorField> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
