package com.ssr.entitys;

/**
 * Created by ssr on 26.03.17.
 */
public class ValidationErrorField {
    private final String field;
    private final String message;

    public ValidationErrorField(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public String getMessage() {
        return message;
    }
}
