package com.ssr.entitys.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * Created by ssr on 11.03.17.
 */
@Document(collection = "diffFriends")
public class DiffFriends {
    @org.springframework.data.annotation.Id
    @JsonProperty("userVkId")
    private String userVkId;
    @JsonProperty("diffs")
    private List<Diff> diffs;

    public DiffFriends() {
    }

    public DiffFriends(String userVkId, List<Diff> diffs) {
        this.userVkId = userVkId;
        this.diffs = diffs;
    }

    public List<Diff> getDiffs() {
        return diffs;
    }

    public void setDiffs(List<Diff> diffs) {
        this.diffs = diffs;
    }

    public String getUserVkId() {
        return userVkId;
    }

    public void setUserVkId(String userVkId) {
        this.userVkId = userVkId;
    }


    public static class Diff {
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd,HH:00", timezone = "MSK")
        private Date timestamp;
        @JsonProperty("diffFriends")
        private List<String> diffFriends;

        public Diff() {
        }

        public Diff(List<String> diffFriends) {
            this.diffFriends = diffFriends;
            this.timestamp = Date.from(Instant.now());
        }

        public Date getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(Date timestamp) {
            this.timestamp = timestamp;
        }

        public List<String> getDiffFriends() {
            return diffFriends;
        }

        public void setDiffFriends(List<String> diffFriends) {
            this.diffFriends = diffFriends;
        }
    }
}
