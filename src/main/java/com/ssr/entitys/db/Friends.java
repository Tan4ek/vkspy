package com.ssr.entitys.db;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * Created by ssr on 07.03.17.
 */

@Document(collection = "friends")
public class Friends {
    @org.springframework.data.annotation.Id
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("friends")
    private List<Integer> friends;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd,HH:00", timezone = "MSK")
    private Date timestamp;

    public Friends() {
        timestamp = Date.from(Instant.now());
    }

    public Friends(String userId, List<Integer> friends) {
        this();
        this.userId = userId;
        this.friends = friends;
    }

    public Friends(Friends clone) {
        this(clone.getUserId(), clone.getFriends());
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<Integer> getFriends() {
        return friends;
    }

    public void setFriends(List<Integer> friends) {
        this.friends = friends;
    }
}
