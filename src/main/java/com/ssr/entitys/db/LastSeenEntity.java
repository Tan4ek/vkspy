package com.ssr.entitys.db;

import com.vk.api.sdk.objects.users.LastSeen;

import java.util.Date;
import java.util.Objects;

/**
 * Created by ssr on 16.03.17.
 */
public class LastSeenEntity {
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd,HH:00", timezone = "MSK")
    private Date timestamp;
    private LastSeen lastSeen;
    private boolean isOnline;
    private boolean onlineMobile;
    private Integer onlineApp;

    public LastSeenEntity() {
    }

    public LastSeenEntity(LastSeen lastSeen, boolean isOnline, boolean onlineMobile, Integer onlineApp) {
        this.lastSeen = lastSeen;
        this.timestamp = new Date();
        this.isOnline = isOnline;
        this.onlineMobile = onlineMobile;
        this.onlineApp = onlineApp;
    }

    public LastSeen getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(LastSeen lastSeen) {
        this.lastSeen = lastSeen;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public boolean isOnlineMobile() {
        return onlineMobile;
    }

    public void setOnlineMobile(boolean onlineMobile) {
        this.onlineMobile = onlineMobile;
    }

    public Integer getOnlineApp() {
        return onlineApp;
    }

    public void setOnlineApp(Integer onlineApp) {
        this.onlineApp = onlineApp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LastSeenEntity that = (LastSeenEntity) o;
        return isOnline == that.isOnline &&
                onlineMobile == that.onlineMobile &&
                Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(lastSeen, that.lastSeen) &&
                Objects.equals(onlineApp, that.onlineApp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, lastSeen, isOnline, onlineMobile, onlineApp);
    }

    @Override
    public String toString() {
        if (lastSeen != null) {
            return "" + lastSeen.getTime();
        }
        return "--";
    }
}
