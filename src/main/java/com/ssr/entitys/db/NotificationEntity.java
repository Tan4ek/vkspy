package com.ssr.entitys.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by ssr on 25.03.17.
 */
public class NotificationEntity implements Serializable {
    private static final long serialVersionUID = 6789084989262617667L;
    @Id
    @Field("hash")
    @JsonProperty("hash")
    private String hash;
    @Field("notificationMail")
    @JsonProperty("notificationMail")
    @Indexed
    private String notificationMail;
    @Field("sendOnlineReport")
    @JsonProperty("sendOnlineReport")
    private boolean sendOnlineReport;
    @Field("sendChangeFriendsReport")
    @JsonProperty("sendChangeFriendsReport")
    private boolean sendChangeFriendsReport;

    public NotificationEntity() {
    }

    @PersistenceConstructor
    public NotificationEntity(String hash, String notificationMail, boolean sendOnlineReport, boolean sendChangeFriendsReport) {
        this.hash = hash;
        this.notificationMail = notificationMail;
        this.sendOnlineReport = sendOnlineReport;
        this.sendChangeFriendsReport = sendChangeFriendsReport;
    }


    public void setHash(String hash) {
        this.hash = hash;
    }

    public void setNotificationMail(String notificationMail) {
        this.notificationMail = notificationMail;
    }

    public void setSendOnlineReport(boolean sendOnlineReport) {
        this.sendOnlineReport = sendOnlineReport;
    }

    public void setSendChangeFriendsReport(boolean sendChangeFriendsReport) {
        this.sendChangeFriendsReport = sendChangeFriendsReport;
    }

    public String getHash() {
        return hash;
    }

    public String getNotificationMail() {
        return notificationMail;
    }

    public boolean isSendOnlineReport() {
        return sendOnlineReport;
    }

    public boolean isSendChangeFriendsReport() {
        return sendChangeFriendsReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationEntity that = (NotificationEntity) o;
        return Objects.equals(hash, that.hash) &&
                Objects.equals(notificationMail, that.notificationMail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hash, notificationMail);
    }
}
