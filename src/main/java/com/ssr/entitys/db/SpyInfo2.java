package com.ssr.entitys.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vk.api.sdk.objects.users.UserFull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Created by ssr on 25.03.17.
 */
@Document(collection = "spyInfo2")
public class SpyInfo2 implements Serializable{
    private static final long serialVersionUID = -8934185602660804649L;
    @Id
    @JsonProperty("vkId")
    @NotNull
    private String vkId;
    @JsonProperty("notificationEntities")
    @Field("notificationEntities")
    @NotEmpty
    private List<NotificationEntity > notificationEntities;

    @JsonProperty("vkUser")
    private UserFull vkUser;


    public List<NotificationEntity> getNotificationEntities() {
        return notificationEntities;
    }

    public void setNotificationEntities(List<NotificationEntity> notificationEntities) {
        this.notificationEntities = notificationEntities;
    }

    public UserFull getVkUser() {
        return vkUser;
    }

    public void setVkUser(UserFull vkUser) {
        this.vkUser = vkUser;
    }

    public String  getVkId() {
        return vkId;
    }

    public void setVkId(String  vkId) {
        this.vkId = vkId;
    }
}
