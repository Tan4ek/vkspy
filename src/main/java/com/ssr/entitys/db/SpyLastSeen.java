package com.ssr.entitys.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by ssr on 16.03.17.
 */
@Document(collection = "spyLastSeen")
public class SpyLastSeen {
    @org.springframework.data.annotation.Id
    private String vkId;

    @JsonProperty("lastSeen")
    private List<LastSeenEntity> lastSeen;

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public List<LastSeenEntity> getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(List<LastSeenEntity> lastSeen) {
        this.lastSeen = lastSeen;
    }
}
