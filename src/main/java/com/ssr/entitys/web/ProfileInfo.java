package com.ssr.entitys.web;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ssr on 02.04.17.
 */
public class ProfileInfo {
    @JsonProperty("notificationMail")
    private String notificationMail;
    @JsonProperty("profileEntity")
    private ProfileNotificationEntity profileEntity;


    public ProfileNotificationEntity getProfileEntity() {
        return profileEntity;
    }

    public void setProfileEntity(ProfileNotificationEntity profileEntity) {
        this.profileEntity = profileEntity;
    }

    public String getNotificationMail() {
        return notificationMail;
    }

    public void setNotificationMail(String notificationMail) {
        this.notificationMail = notificationMail;
    }

}
