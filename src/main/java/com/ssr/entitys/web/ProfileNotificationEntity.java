package com.ssr.entitys.web;

import com.ssr.entitys.db.NotificationEntity;
import com.vk.api.sdk.objects.users.UserFull;

/**
 * Created by ssr on 02.04.17.
 */
public class ProfileNotificationEntity extends NotificationEntity {
    private static final long serialVersionUID = 5370341033381724657L;
    private String vkId;
    private UserFull user;

    public ProfileNotificationEntity(NotificationEntity notificationEntity) {
        super(notificationEntity.getHash(), notificationEntity.getNotificationMail(), notificationEntity.isSendOnlineReport(), notificationEntity.isSendChangeFriendsReport());
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public UserFull getUser() {
        return user;
    }

    public void setUser(UserFull user) {
        this.user = user;
    }
}
