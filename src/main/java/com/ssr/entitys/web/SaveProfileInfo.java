package com.ssr.entitys.web;

/**
 * Created by ssr on 02.04.17.
 */
public class SaveProfileInfo {
//    hash
    private String profileId;
    private boolean sendChangeFriendsReport;
    private boolean sendOnlineReport;

    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public boolean isSendChangeFriendsReport() {
        return sendChangeFriendsReport;
    }

    public void setSendChangeFriendsReport(boolean sendChangeFriendsReport) {
        this.sendChangeFriendsReport = sendChangeFriendsReport;
    }

    public boolean isSendOnlineReport() {
        return sendOnlineReport;
    }

    public void setSendOnlineReport(boolean sendOnlineReport) {
        this.sendOnlineReport = sendOnlineReport;
    }
}
