package com.ssr.entitys.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by ssr on 25.03.17.
 */
public class SubscribeVkSpy {
    @JsonProperty("vkId")
    @NotBlank
    private String vkId;
    @JsonProperty("notificationMail")
    @NotBlank
    @Email
    private String notificationMail;
    @JsonProperty("sendOnlineReport")
    private boolean sendOnlineReport;
    @JsonProperty("sendChangeFriendsReport")
    private boolean sendChangeFriendsReport;

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public String getNotificationMail() {
        return notificationMail;
    }

    public void setNotificationMail(String notificationMail) {
        this.notificationMail = notificationMail;
    }

    public boolean isSendOnlineReport() {
        return sendOnlineReport;
    }

    public void setSendOnlineReport(boolean sendOnlineReport) {
        this.sendOnlineReport = sendOnlineReport;
    }

    public boolean isSendChangeFriendsReport() {
        return sendChangeFriendsReport;
    }

    public void setSendChangeFriendsReport(boolean sendChangeFriendsReport) {
        this.sendChangeFriendsReport = sendChangeFriendsReport;
    }
}
