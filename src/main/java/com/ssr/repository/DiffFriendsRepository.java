package com.ssr.repository;

import com.ssr.entitys.db.DiffFriends;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ssr on 11.03.17.
 */
public interface DiffFriendsRepository extends MongoRepository<DiffFriends, String>, DiffFriendsRepositoryCustom {
}
