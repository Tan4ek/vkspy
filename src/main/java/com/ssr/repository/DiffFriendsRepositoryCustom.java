package com.ssr.repository;

import com.ssr.entitys.db.DiffFriends;

/**
 * Created by ssr on 11.03.17.
 */
public interface DiffFriendsRepositoryCustom {
    void pushToDiffFriends(String userVkId, DiffFriends.Diff diff);
}
