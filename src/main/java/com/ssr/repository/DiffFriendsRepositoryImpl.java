package com.ssr.repository;

import com.ssr.entitys.db.DiffFriends;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * Created by ssr on 11.03.17.
 */
public class DiffFriendsRepositoryImpl implements DiffFriendsRepositoryCustom {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void pushToDiffFriends(String userVkId, DiffFriends.Diff diff) {
        mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(userVkId)), new Update().push("diffs", diff), "diffFriends");
    }
}
