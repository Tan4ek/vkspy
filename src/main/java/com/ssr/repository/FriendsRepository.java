package com.ssr.repository;

import com.ssr.entitys.db.Friends;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ssr on 07.03.17.
 */

public interface FriendsRepository extends MongoRepository<Friends, String>{
}
