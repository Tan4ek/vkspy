package com.ssr.repository;

import com.ssr.entitys.db.SpyInfo2;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ssr on 25.03.17.
 */
public interface SpyInfo2Repository extends MongoRepository<SpyInfo2, String> , SpyInfo2RepositoryCustom{
}
