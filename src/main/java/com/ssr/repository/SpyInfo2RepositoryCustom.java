package com.ssr.repository;

import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;

import javax.annotation.Nonnull;

/**
 * Created by ssr on 25.03.17.
 */
public interface SpyInfo2RepositoryCustom {
    SpyInfo2 findByHash(@Nonnull String hash);

    boolean pullNotificationEntity(String hash);

    void pushNotificationEntity(@Nonnull String vkId, @Nonnull NotificationEntity notificationEntity);

    boolean existNotificationEmail(@Nonnull String vkId, @Nonnull String notificationMail);
}
