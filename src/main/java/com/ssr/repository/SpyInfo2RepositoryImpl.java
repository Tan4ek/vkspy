package com.ssr.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.WriteResult;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import javax.annotation.Nonnull;

/**
 * Created by ssr on 25.03.17.
 */
public class SpyInfo2RepositoryImpl implements SpyInfo2RepositoryCustom {
    private final MongoTemplate mongoTemplate;

    public SpyInfo2RepositoryImpl(@Autowired MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public SpyInfo2 findByHash(@Nonnull String hash) {
        return mongoTemplate.findOne(Query.query(Criteria.where("notificationEntities.hash").is(hash)), SpyInfo2.class);
    }

    @Override
    public boolean pullNotificationEntity(String hash) {
        WriteResult writeResult = mongoTemplate.updateFirst(Query.query(Criteria.where("notificationEntities.hash").is(hash)), new Update().pull("notificationEntities", new BasicDBObject("hash", hash)), SpyInfo2.class);
        return writeResult.isUpdateOfExisting();
    }

    @Override
    public void pushNotificationEntity(@Nonnull String vkId, @Nonnull NotificationEntity notificationEntity) {
        mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(vkId)), new Update().push("notificationEntities", notificationEntity), SpyInfo2.class);
    }

    @Override
    public boolean existNotificationEmail(@Nonnull String vkId, @Nonnull String notificationMail) {
        return mongoTemplate.exists(Query.query(Criteria.where("_id").is(vkId).andOperator(Criteria.where("notificationEntities").elemMatch(Criteria.where("notificationMail").is(notificationMail)))), SpyInfo2.class);
    }
}
