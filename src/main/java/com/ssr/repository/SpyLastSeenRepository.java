package com.ssr.repository;

import com.ssr.entitys.db.SpyLastSeen;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ssr on 16.03.17.
 */
public interface SpyLastSeenRepository extends MongoRepository<SpyLastSeen, String>, SpyLastSeenRepositoryCustom {
}
