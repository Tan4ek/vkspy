package com.ssr.repository;

import com.ssr.entitys.db.LastSeenEntity;
import com.ssr.entitys.db.SpyLastSeen;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

/**
 * Created by ssr on 16.03.17.
 */
public interface SpyLastSeenRepositoryCustom {
    void putLastSeenEntity(String vkId, LastSeenEntity lastSeenEntity);

    List<SpyLastSeen> findBetweenDate(Instant startDate, Instant endDate, Collection<String> vkIds);
}
