package com.ssr.repository;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.ssr.entitys.db.LastSeenEntity;
import com.ssr.entitys.db.SpyLastSeen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.sql.Date;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by ssr on 16.03.17.
 */
public class SpyLastSeenRepositoryImpl implements SpyLastSeenRepositoryCustom {
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void putLastSeenEntity(String userVkId, LastSeenEntity lastSeenEntity) {
        mongoTemplate.updateFirst(Query.query(Criteria.where("_id").is(userVkId)), new Update().push("lastSeen", lastSeenEntity), "spyLastSeen");
    }

    @Override
    public List<SpyLastSeen> findBetweenDate(Instant startDate, Instant endDate, Collection<String> vkIds) {
        //todo ждать релиза spring-data-mongodb
        ProjectionOperation as = Aggregation.project()
                .and(context -> {
                    DBObject filterExpression = new BasicDBObject();
                    filterExpression.put("input", "$lastSeen");
                    filterExpression.put("as", "lastSeen");
                    filterExpression.put("cond", new BasicDBObject("$and",
                            Arrays.<Object>asList(
                                    new BasicDBObject("$gte", Arrays.<Object>asList("$$lastSeen.timestamp", Date.from(startDate))),
                                    new BasicDBObject("$lt", Arrays.<Object>asList("$$lastSeen.timestamp", Date.from(endDate))))));
                    return new BasicDBObject("$filter", filterExpression);
                }).as("lastSeen");

        AggregationResults<SpyLastSeen> spyLastSeen = mongoTemplate.aggregate(Aggregation.newAggregation(as,
                Aggregation.match(Criteria.where("_id").in(vkIds))), "spyLastSeen", SpyLastSeen.class);
        return spyLastSeen.getMappedResults();
    }


}
