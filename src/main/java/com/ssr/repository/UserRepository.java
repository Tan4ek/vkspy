package com.ssr.repository;

import com.ssr.entitys.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by ssr on 18.01.17.
 */
public interface UserRepository extends MongoRepository<User, String> {

}
