package com.ssr.services;

import com.ssr.beans.DomainConfiguration;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ssr on 09.03.17.
 */
@Service
public class MailService implements ApplicationListener<ContextClosedEvent> {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private DomainConfiguration domainConfiguration;

    private ExecutorService executorService;

    @Autowired
    private TemplateEngine templateEngine;

    @PostConstruct
    protected void postConstruct() {
        executorService = Executors.newFixedThreadPool(1);
    }

    public void sendEmail(String to, String subject, String text) {
        if (StringUtils.isBlank(to)) {
            throw new IllegalArgumentException("MailService to is blank");
        }
        SimpleMailMessage crunchifyMsg = new SimpleMailMessage();
        crunchifyMsg.setFrom("sender.do.not.answer@gmail.com");
        crunchifyMsg.setTo(to);
        crunchifyMsg.setSubject(subject);
        crunchifyMsg.setText(text);
        executorService.submit(new SendEmailTask(javaMailSender, crunchifyMsg));
    }

    public void sendEmail(List<String> mails, String subject, String text) {
        if (CollectionUtils.isEmpty(mails)) {
            return;
        }
        for (String mail : mails) {
            sendEmail(mail, subject, text);
        }
    }

    public String generate(ModelAndView modelAndView) {
        Assert.notNull(modelAndView);
        Context context = new Context();
        context.setVariables(modelAndView.getModel());
        return templateEngine.process(modelAndView.getViewName(), context);
    }

    public boolean sendHtmlEmail(String to, String subject, String html) {
        if (StringUtils.isBlank(to)) {
            throw new IllegalArgumentException("MailService to is blank");
        }
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, false, StandardCharsets.UTF_8.name());
            mimeMessage.setText(html, StandardCharsets.UTF_8.name(), "html");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setFrom("sender.do.not.answer@gmail.com");
            executorService.submit(new SendEmailTask(javaMailSender, mimeMessage));
            return true;
        } catch (MessagingException e) {
            logger.error("Can't send email to " + to + " with subject " + subject, e);
            return false;
        }
    }

    public void sendHtmlEmail(List<String> to, String subject, String html) {
        if (CollectionUtils.isEmpty(to)) {
            return;
        }
        for (String sendTo : to) {
            sendHtmlEmail(sendTo, subject, html);
        }
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        if (executorService != null) {
            //TODO samohvalov: отправлять неотравленные сообщения после включения
            executorService.shutdownNow();
        }
    }

    public static class SendEmailTask implements Runnable {
        private final JavaMailSender javaMailSender;
        private final MimeMessage mimeMessage;
        private final SimpleMailMessage simpleMailMessage;

        public SendEmailTask(@Nonnull JavaMailSender javaMailSender, MimeMessage mimeMessage) {
            Assert.notNull(javaMailSender);
            this.javaMailSender = javaMailSender;
            this.mimeMessage = mimeMessage;
            this.simpleMailMessage = null;
        }

        public SendEmailTask(@Nonnull JavaMailSender javaMailSender, SimpleMailMessage simpleMailMessage) {
            Assert.notNull(javaMailSender);
            this.javaMailSender = javaMailSender;
            this.simpleMailMessage = simpleMailMessage;
            this.mimeMessage = null;
        }

        @Override
        public void run() {
            if (mimeMessage != null) {
                javaMailSender.send(mimeMessage);
            } else if (simpleMailMessage != null) {
                javaMailSender.send(simpleMailMessage);
            }
        }
    }

}
