package com.ssr.services;

import com.ssr.entitys.db.LastSeenEntity;
import com.ssr.entitys.db.SpyLastSeen;
import com.ssr.repository.SpyLastSeenRepository;
import com.ssr.utils.VkUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

/**
 * Created by ssr on 07.03.17.
 */
@Service
public class SpyService {
    private static final CSVFormat DEFAULT_FORMAT = CSVFormat.EXCEL;
    private static final DateTimeFormatter DEFAULT_DATE_TIME_FORMATTER = DateTimeFormatter.ISO_DATE_TIME.withZone(VkUtils.MOSCOW_ZONE);

    @Autowired
    private SpyLastSeenRepository lastSeenRepository;


    public String getCsvVisitation(String vkId) throws IOException {
        SpyLastSeen spyLastSeen = lastSeenRepository.findOne(vkId);
        StringBuilder stringBuilder = new StringBuilder();

        CSVPrinter printer = new CSVPrinter(stringBuilder, DEFAULT_FORMAT);

        printer.printRecord("vk_id", "is_online", "is_online_mobile", "timestamp", "last_seen", "platform_id");

        for (LastSeenEntity lastSeen : spyLastSeen.getLastSeen()) {
            printer.printRecord(spyLastSeen.getVkId(),
                    lastSeen.isOnline(),
                    lastSeen.isOnlineMobile(),
                    DEFAULT_DATE_TIME_FORMATTER.format(lastSeen.getTimestamp().toInstant()),
                    DEFAULT_DATE_TIME_FORMATTER.format(Instant.ofEpochSecond(lastSeen.getLastSeen().getTime())),
                    lastSeen.getLastSeen().getPlatform());
        }

        return stringBuilder.toString();
    }


}
