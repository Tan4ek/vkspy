package com.ssr.services;

import com.ssr.beans.DomainConfiguration;
import com.ssr.entitys.LastSeenTimestamp;
import com.ssr.entitys.LastSeenTimestampPeriod;
import com.ssr.entitys.User;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.entitys.db.SpyLastSeen;
import com.ssr.repository.SpyLastSeenRepository;
import com.ssr.repository.UserRepository;
import com.ssr.utils.TimeUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.ssr.utils.VkUtils.MOSCOW_ZONE;

/**
 * Created by ssr on 18.01.17.
 */
@Service
public class UserService {
    private final static Logger logger = LoggerFactory.getLogger(UserService.class);
    @Autowired
    private SpyLastSeenRepository spyLastSeenRepository;
    @Autowired
    private MailService mailService;

    @Autowired
    private DomainConfiguration domainConfiguration;

    private final UserRepository userRepository;

    public UserService(@Autowired UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static List<LastSeenTimestampPeriod> getLastSeenTimestampPeriods(List<LastSeenTimestamp> lastSeenTimestamps) {
        if (CollectionUtils.isEmpty(lastSeenTimestamps)) {
            return Collections.emptyList();
        }

        LastSeenTimestamp start = null, lastTimestamp = null;
        List<LastSeenTimestampPeriod> onlinePeriods = new ArrayList<>();

        for (LastSeenTimestamp seenTimestamp : lastSeenTimestamps) {
            boolean wasOnline = start != null;
            if (!wasOnline && seenTimestamp.isOnline()) {
                start = seenTimestamp;
            } else if (wasOnline && !seenTimestamp.isOnline()) {
                onlinePeriods.add(generateLastSeenTimestamp(start, lastTimestamp));
                start = null;
            }
            lastTimestamp = seenTimestamp;
        }

        if (start != null && start == lastTimestamp) {
            onlinePeriods.add(generateLastSeenTimestamp(start));
        } else if (start != null) {
            //если не найден последний оффлайн
            onlinePeriods.add(generateLastSeenTimestamp(start, lastTimestamp));
        }
        return onlinePeriods;
    }

    private static LastSeenTimestampPeriod generateLastSeenTimestamp(LastSeenTimestamp once) {
        return generateLastSeenTimestamp(once, once);
    }

    private static LastSeenTimestampPeriod generateLastSeenTimestamp(LastSeenTimestamp start, LastSeenTimestamp lastTimestamp) {
        if (start == lastTimestamp || start.equals(lastTimestamp)) {
            if (start.getTimestamp().compareTo(start.getLastSeenInstant()) > 0) {
                LastSeenTimestamp onceSeenTimestamp = new LastSeenTimestamp(start.getSpyLastSeenEntity(), true, start.getLastSeenInstant());
                return new LastSeenTimestampPeriod(onceSeenTimestamp);
            } else {
                return new LastSeenTimestampPeriod(start);
            }
        }
        return new LastSeenTimestampPeriod(start, checkOffline(start, lastTimestamp));
    }

    private static LastSeenTimestamp checkOffline(LastSeenTimestamp start, LastSeenTimestamp lastTimestamp) {
        boolean lastTimestampMoreThenLastSeenTimestamp = lastTimestamp.getTimestamp().compareTo(lastTimestamp.getLastSeenInstant()) > 0;
        boolean lastSeenTimestampMoreThenStartTimestamp = lastTimestamp.getLastSeenInstant().compareTo(start.getTimestamp()) > 0;

        if (lastTimestampMoreThenLastSeenTimestamp && lastSeenTimestampMoreThenStartTimestamp) {
            return new LastSeenTimestamp(lastTimestamp.getSpyLastSeenEntity(), true, lastTimestamp.getLastSeenInstant());
        }
        return lastTimestamp;
    }

    public User registrationUser(User user) {
        return userRepository.insert(user);
    }

    public void sendLastSeenEmails(List<SpyInfo2> spyInfos, LocalDateTime todayMidnight, LocalDateTime tomorrowMidnight, Predicate<NotificationEntity> filter) {
        List<SpyLastSeen> spyLastSeens = spyLastSeenRepository.findBetweenDate(todayMidnight.atZone(MOSCOW_ZONE).toInstant(),
                tomorrowMidnight.atZone(MOSCOW_ZONE).toInstant(),
                spyInfos.stream().map((spyInfo2) -> "" + spyInfo2.getVkUser().getId()).collect(Collectors.toList()));

        Map<String, SpyInfo2> vkIdToSpyInfo = spyInfos.stream().collect(Collectors.toMap((spyInfo2) -> "" + spyInfo2.getVkUser().getId(), Function.identity()));


        for (SpyLastSeen spyLastSeen : spyLastSeens) {
            List<LastSeenTimestamp> lastSeenTimestamps = spyLastSeen.getLastSeen().stream()
                    .map(LastSeenTimestamp::of)
                    .sorted(Comparator.comparing(LastSeenTimestamp::getTimestamp))
                    .collect(Collectors.toList());

            List<LastSeenTimestampPeriod> onlinePeriods = getLastSeenTimestampPeriods(lastSeenTimestamps);

            SpyInfo2 spyInfo = vkIdToSpyInfo.get(spyLastSeen.getVkId());
            for (NotificationEntity notificationEntity : spyInfo.getNotificationEntities()) {
                if (!filter.test(notificationEntity)) {
                    continue;
                }

                Function<LastSeenTimestampPeriod, Duration> periodToDuration = (period) -> {
                    if (!period.isEmptyPeriod()) {
                        return Duration.between(period.getStart().getTimestamp(), period.getFinish().getTimestamp());
                    }
                    return Duration.ofMinutes(1);
                };

                Duration allDuration = onlinePeriods.stream().map(periodToDuration).reduce(Duration.ZERO, Duration::plus);
                Map<String, Object> modal = new HashMap<>();
                modal.put("today", todayMidnight.toLocalDate().format(DateTimeFormatter.ISO_DATE));
                modal.put("user", spyInfo.getVkUser());
                modal.put("onlinePeriods", onlinePeriods);
                modal.put("allOnlineTime", TimeUtils.durationToString(allDuration));
                domainConfiguration.fillNotificationProfile(modal, notificationEntity);
                ModelAndView modelAndView = new ModelAndView("mails/onlineReport", modal);
                String html = mailService.generate(modelAndView);
                logger.debug("send online report to " + notificationEntity.getNotificationMail());
                mailService.sendHtmlEmail(notificationEntity.getNotificationMail(), "Online report", html);
            }
        }
    }

}
