package com.ssr.sheduling;

import com.ssr.Application;
import com.ssr.beans.DomainConfiguration;
import com.ssr.entitys.DiffFriendsMailEntity;
import com.ssr.entitys.db.DiffFriends;
import com.ssr.entitys.db.Friends;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.repository.DiffFriendsRepository;
import com.ssr.repository.FriendsRepository;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.services.MailService;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.friends.responses.GetResponse;
import com.vk.api.sdk.objects.users.User;
import com.vk.api.sdk.objects.users.UserFull;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by ssr on 07.03.17.
 */
@Component("vkCheckFriends")
public class CheckVkFriendJob {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private VkApiClient vkApiClient;
    @Autowired
    private FriendsRepository friendsRepository;
    @Autowired
    private MailService mailService;
    @Autowired
    private SpyInfo2Repository spyInfo2Repository;
    @Autowired
    private DiffFriendsRepository diffFriendsRepository;
    @Autowired
    private Application.JobWorker jobWorker;
    @Autowired
    private DomainConfiguration domainConfiguration;

    @PostConstruct
    public void postContract() {
        Assert.notNull(vkApiClient);
        Assert.notNull(friendsRepository);
        Assert.notNull(mailService);
        Assert.notNull(spyInfo2Repository);
        Assert.notNull(diffFriendsRepository);
        Assert.notNull(jobWorker);
        Assert.notNull(domainConfiguration);
    }

    //invoke method, do not rename
    public void checkFriends() throws JobExecutionException {
        if (jobWorker.skipJob) {
            return;
        }
        try {
            logger.debug("run checkFriends");
            List<SpyInfo2> all1 = spyInfo2Repository.findAll();

            for (SpyInfo2 spyInfo : all1) {
                logger.debug("spyInfo check id: " + spyInfo.getVkId());
                GetResponse response = vkApiClient.friends().get().userId(spyInfo.getVkUser().getId()).execute();

                List<Integer> currentFriends = response.getItems();

                String userId = "" + spyInfo.getVkUser().getId();
                Friends friends = friendsRepository.findOne(userId);
                if (friends == null) {
                    Friends friends1 = new Friends(userId, currentFriends);
                    friendsRepository.insert(friends1);
                } else {
                    FriendsChange addedRemovedDiff = diff(friends.getFriends(), currentFriends);
                    if (CollectionUtils.isNotEmpty(addedRemovedDiff.getDiff())) {
                        List<String> diffFriends = addedRemovedDiff.getDiff().stream().map(s -> "" + s).collect(Collectors.toList());
                        logger.debug("diffFriends for " + userId + " " + Date.from(Instant.now()).toString() + " " + diffFriends);

                        if (diffFriendsRepository.exists(userId)) {
                            diffFriendsRepository.pushToDiffFriends(userId, new DiffFriends.Diff(diffFriends));
                        } else {
                            diffFriendsRepository.insert(new DiffFriends(userId, Collections.singletonList(new DiffFriends.Diff(diffFriends))));
                        }

                        List<UserXtrCounters> execute = vkApiClient.users().get().userIds(diffFriends).fields(UserField.PHOTO_50).execute();

                        List<User> added = new ArrayList<>();
                        List<User> removed = new ArrayList<>();

                        for (UserXtrCounters user : execute) {
                            if (addedRemovedDiff.getAdded().contains(user.getId())) {
                                added.add(user);
                            } else {
                                removed.add(user);
                            }
                        }

                        for (NotificationEntity notificationEntity : spyInfo.getNotificationEntities().stream().filter(NotificationEntity::isSendChangeFriendsReport).collect(Collectors.toList())) {
                            Map<String, Object> modal = new HashMap<>();
                            modal.put("user",
                                    new DiffFriendsMailEntity(spyInfo.getVkUser(), execute.stream().map(u -> (User) u).collect(Collectors.toList()), added, removed));
                            ModelAndView modelAndView = new ModelAndView("mails/diffFriendsTemplate", modal);
                            String html = mailService.generate(modelAndView);
                            domainConfiguration.fillNotificationProfile(modal, notificationEntity);
                            logger.debug("send email to " + notificationEntity.getNotificationMail());
                            mailService.sendHtmlEmail(notificationEntity.getNotificationMail(), "New friends", html);
                        }

                        friendsRepository.save(new Friends(friends.getUserId(), currentFriends));
                    }
                }
            }
        } catch (ApiException | ClientException e) {
            logger.error("Не удалось проверить друзей!", e);
        }
    }

    private static String mailMessage(List<? extends UserFull> friendsDiff, UserFull user) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Для пользователя: ").append(user.getFirstName()).append(" ").append(user.getLastName()).append(" ")
                .append("https://vk.com/id").append(user.getId()).append(" добавились:").append("\n");
        for (UserFull newFriend : friendsDiff) {
            stringBuilder.append("-").append(newFriend.getFirstName()).append(" ").append(newFriend.getLastName()).append(" ").append("https://vk.com/id").append(newFriend.getId()).append("\n");
        }
        return stringBuilder.toString();
    }


    //left - added, middle - removed, disjunction - right
    public static FriendsChange diff(List<Integer> source, List<Integer> newSource) {
        HashSet<Integer> setSource = new HashSet<>(source);

        Collection<Integer> disjunction = CollectionUtils.disjunction(source, newSource);

        List<Integer> added = new LinkedList<>();
        List<Integer> removed = new LinkedList<>();

        for (Integer integer : disjunction) {
            if (setSource.contains(integer)) {
                removed.add(integer);
            } else {
                added.add(integer);
            }
        }

        return new FriendsChange(disjunction, added, removed);
    }

    private static final class FriendsChange {
        private final Triple<Set<Integer>, Set<Integer>, Set<Integer>> triple;

        FriendsChange(Collection<Integer> diff, Collection<Integer> added, Collection<Integer> removed) {
            this.triple = ImmutableTriple.of(new HashSet<>(diff), new HashSet<>(added), new HashSet<>(removed));
        }

        public Set<Integer> getDiff() {
            return triple.getLeft();
        }

        public Set<Integer> getAdded() {
            return triple.getMiddle();
        }

        public Set<Integer> getRemoved() {
            return triple.getRight();
        }
    }
}
