package com.ssr.sheduling;

import com.ssr.Application;
import com.ssr.entitys.db.LastSeenEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.entitys.db.SpyLastSeen;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.repository.SpyLastSeenRepository;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.users.UserField;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ssr on 16.03.17.
 */
@Component("lastSeenVkJob")
public class LastSeenVkJob {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private VkApiClient vkApiClient;

    @Autowired
    private SpyInfo2Repository spyInfo2Repository;

    @Autowired
    private SpyLastSeenRepository spyLastSeenRepository;

    @Autowired
    private Application.JobWorker jobWorker;

    @PostConstruct
    public void init() {
        Assert.notNull(vkApiClient);
        Assert.notNull(spyInfo2Repository);
        Assert.notNull(spyLastSeenRepository);
        Assert.notNull(jobWorker);
    }

    //не переименовывать
    public void checkLastSeen() throws ClientException, ApiException {
        if (jobWorker.skipJob) {
            return;
        }
        logger.debug("Стартовал last seen job");

        List<SpyInfo2> all = spyInfo2Repository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return;
        }

        List<String> usersVkIds = all.stream().map((spyInfo2) -> "" + spyInfo2.getVkUser().getId()).collect(Collectors.toList());


        List<UserXtrCounters> execute = vkApiClient.users().get().userIds(usersVkIds).fields(UserField.LAST_SEEN, UserField.ONLINE).execute();

        if (CollectionUtils.isEmpty(execute)) {
            logger.error("Из базы вернулся пустой список для проверки lastSeen");
            return;
        }

        for (UserXtrCounters user : execute) {
            Integer vkId = user.getId();
            boolean userExist = spyLastSeenRepository.exists("" + vkId);

            boolean isOnline = user.isOnline();
            boolean onlineMobile = user.isOnlineMobile();
            Integer onlineApp = user.getOnlineApp();
            if (!userExist) {
                SpyLastSeen spyLastSeen = new SpyLastSeen();
                spyLastSeen.setVkId("" + vkId);
                LastSeenEntity lastSeenEntity = new LastSeenEntity(user.getLastSeen(), isOnline, onlineMobile, onlineApp);
                spyLastSeen.setLastSeen(Collections.singletonList(lastSeenEntity));
                spyLastSeenRepository.save(spyLastSeen);
            } else {
                spyLastSeenRepository.putLastSeenEntity("" + vkId, new LastSeenEntity(user.getLastSeen(), isOnline, onlineMobile, onlineApp));
            }
        }
    }
}
