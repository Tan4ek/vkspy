package com.ssr.sheduling;

import com.ssr.Application;
import com.ssr.beans.DomainConfiguration;
import com.ssr.entitys.db.NotificationEntity;
import com.ssr.entitys.db.SpyInfo2;
import com.ssr.repository.SpyInfo2Repository;
import com.ssr.repository.SpyLastSeenRepository;
import com.ssr.services.MailService;
import com.ssr.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.ssr.utils.VkUtils.MOSCOW_ZONE;

/**
 * Created by ssr on 19.03.17.
 */
@Component("reportLastSeenJob")
public class ReportLastSeenJob {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SpyInfo2Repository spyInfo2Repository;
    @Autowired
    private SpyLastSeenRepository spyLastSeenRepository;
    @Autowired
    private Application.JobWorker jobWorker;
    @Autowired
    private MailService mailService;

    @Autowired
    private DomainConfiguration domainConfiguration;
    @Autowired
    private UserService userService;

    @PostConstruct
    public void init() {
        Assert.notNull(domainConfiguration);
        Assert.notNull(spyInfo2Repository);
        Assert.notNull(spyLastSeenRepository);
        Assert.notNull(jobWorker);
        Assert.notNull(mailService);
        Assert.notNull(userService);
    }

    //do not change method name
    public void makeReport() {
        if (jobWorker.skipJob) {
            return;
        }
        logger.debug("Start report lastSeenJob");

        List<SpyInfo2> spyInfos = spyInfo2Repository.findAll().stream()
                .filter(spyInfo2 -> spyInfo2.getNotificationEntities().stream().map(NotificationEntity::isSendOnlineReport).filter(t -> t).findAny().orElse(false))
                .collect(Collectors.toList());

        if (spyInfos.isEmpty()) {
            logger.debug("reportLastSeenJob , empty spyInfo");
            return;
        }
        mailService.sendEmail("ssr205@yandex.ru", "Стартовал отчет online", "Для : " +
                spyInfos.stream().flatMap(spyInfo1 -> spyInfo1.getNotificationEntities().stream().map(NotificationEntity::getNotificationMail)).collect(Collectors.joining(" ")));

        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(MOSCOW_ZONE);
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        LocalDateTime tomorrowMidnight = todayMidnight.plusDays(1);

        userService.sendLastSeenEmails(spyInfos, todayMidnight, tomorrowMidnight, NotificationEntity::isSendOnlineReport);
    }

}
