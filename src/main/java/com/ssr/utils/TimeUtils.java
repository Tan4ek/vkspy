package com.ssr.utils;

import java.time.Duration;

/**
 * Created by ssr on 13.06.17.
 */
public class TimeUtils {
    private TimeUtils() {
        throw new IllegalStateException("private");
    }

    public static String durationToString(Duration duration) {
        if (duration == null || duration.isZero()) {
            return "";
        }
        long days = duration.toDays();
        long hours = duration.toHours() - (24 * duration.toDays());
        long minutes = duration.toMinutes() - (60 * duration.toHours());
        String result = null;
        if (days > 0) {
            result = "" + days + " дней(нь) ";
        }
        if (hours > 0) {
            result = (result == null ? "" : result) + hours + " час(а,ов) ";
        }
        if (minutes > 0) {
            result = (result == null ? "" : result) + "" + minutes + " минут(ы).";
        }

        return result;
    }
}
