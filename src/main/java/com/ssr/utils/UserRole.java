package com.ssr.utils;

/**
 * Created by ssr on 18.01.17.
 */
public enum UserRole {
    ROLE_USER, ROLE_ADMIN
}
