package com.ssr.utils;

import com.vk.api.sdk.queries.users.UserField;

import java.time.ZoneId;

/**
 * Created by ssr on 24.03.17.
 */
public class VkUtils {

    private VkUtils() {
    }

    public static final ZoneId MOSCOW_ZONE = ZoneId.of("Europe/Moscow");

    public final static UserField[] STANDART_USER_FIELDS = new UserField[]{UserField.PHOTO_50, UserField.ABOUT,
            UserField.BDATE, UserField.BOOKS, UserField.CITY, UserField.COUNTRY, UserField.EDUCATION, UserField.HOME_TOWN, UserField.PERSONAL, UserField.RELATION};
}
