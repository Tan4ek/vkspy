package com.ssr.utils.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by ssr on 19.02.17.
 */
@Component //не работает
public class DateToTimestampConverter implements Converter<Date, Timestamp> {
    @Override
    public Timestamp convert(Date source) {
        return Timestamp.from(source.toInstant());
    }
}
