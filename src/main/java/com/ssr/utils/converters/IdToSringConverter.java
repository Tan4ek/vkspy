package com.ssr.utils.converters;

import com.ssr.entitys.Id;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ssr on 19.02.17.
 */
public class IdToSringConverter implements Converter<Id, String> {
    @Override
    public String convert(Id source) {
        return source.getId();
    }
}
