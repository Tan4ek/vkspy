package com.ssr.utils.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by ssr on 05.06.17.
 */
@Component
public final class LocalDateConverter implements Converter<String, LocalDate> {

    private final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;
//
//    public LocalDateConverter(String dateFormat) {
//        this.formatter = DateTimeFormatter.ofPattern(dateFormat);
//    }

    @Override
    public LocalDate convert(String source) {
        if (source == null || source.isEmpty()) {
            return null;
        }

        return LocalDate.parse(source, formatter);
    }
}
