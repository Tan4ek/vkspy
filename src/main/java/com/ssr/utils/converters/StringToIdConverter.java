package com.ssr.utils.converters;

import com.ssr.entitys.Id;
import org.springframework.core.convert.converter.Converter;

/**
 * Created by ssr on 19.02.17.
 */
public class StringToIdConverter implements Converter<String, Id> {
    @Override
    public Id convert(String source) {
        return Id.getInstance(source);
    }
}
