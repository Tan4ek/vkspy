package com.ssr.utils.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Created by ssr on 19.02.17.
 */
@Component
public class StringToTimestampConverter implements Converter<String, Timestamp> {
    @Override
    public Timestamp convert(String source) {
        return Timestamp.valueOf(source);
    }
}
