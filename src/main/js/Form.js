/**
 * Created by ssr on 22.01.17.
 */
import React from "react";

export default class Form extends React.Component {
    render() {
        console.log('props', this.props);
        return (
            <form action={this.props.action} method={this.props.method || null} onSubmit={this.props.onSubmit || null}>
                {this.props.children}
                {document.getElementById('_csrf_parameter_name') != null ?
                    <input type="hidden" name={document.getElementById('_csrf_parameter_name').content}
                           value={document.getElementById('_csrf').content} readOnly="true"/>
                    : null}
            </form>)
    }
}
