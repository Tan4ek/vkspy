'use strict';

import React from "react";
import Form from "./Form";

class LoginField extends React.Component {
    render() {
        return (
            <div>
                <label htmlFor="username">User name</label>
                <input name="username" className="form-control" type="email" placeholder="Email" id="username"/>
            </div>
        )
    }
}

class PassField extends React.Component {
    render() {
        return (
            <div>
                <label htmlFor="password">Password</label>
                <input name="password" className="form-control" type="password" placeholder="Password" id="password"/>
            </div>
        )
    }
}

export default class Login extends React.Component {
    render() {
        return (
            <div className="form-signin" style={{
                'max-width': '330px',
                'padding': '15px',
                'margin': '0 auto'
            }}>
                <Form action="/login1" method="POST">
                    <LoginField/>
                    <PassField/>
                    <button style={{'marginTop': '20px'}} type="submit" className="btn btn-lg btn-primary btn-block">
                        Go!
                    </button>
                </Form>
            </div>
        );
    }
}