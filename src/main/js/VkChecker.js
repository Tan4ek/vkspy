/**
 * Created by ssr on 09.03.17.
 */
import React from "react";
import Form from "./Form";
import Axios from "axios";
import FormSerializer from "form-serialize";
import _ from "lodash";
import FormGroup from "react-bootstrap/lib/FormGroup";
import ControlLabel from "react-bootstrap/lib/ControlLabel";
import FormControl from "react-bootstrap/lib/FormControl";
import HelpBlock from "react-bootstrap/lib/HelpBlock";
import Button from "react-bootstrap/lib/Button";
import Checkbox from "react-bootstrap/lib/Checkbox";
import ButtonToolbar from "react-bootstrap/lib/ButtonToolbar";

export default class VkChecker extends React.Component {
    constructor() {
        super();
        this.state = {errors: {}};

        this.onAnyInput = this.onAnyInput.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onAnyInput(event) {
        if (!_.isEmpty(this.state.errors)) {
            const fieldName = event.target.name;
            this.setState({errors: _.omit(this.state.errors, fieldName)});
        }
    }

    onSubmit(e) {
        e.preventDefault();
        const form = e.currentTarget;
        const serialize = FormSerializer(form);

        Axios.post(form.action, serialize).then((result) => {
            console.log(result);
            location.reload();
        }).catch(error => {
            let fieldErrors = error.response.data.fieldErrors;
            let map = _.reduce(fieldErrors, (obj, itm) => {
                obj[itm.field] = itm.message;
                return obj;
            }, {});
            this.setState({
                errors: map
            });
            console.log(error);
        });
    }

    render() {
        return (
            <div className="form-signin" style={{
                'maxWidth': '330px',
                'padding': '15px',
                'margin': '0 auto'
            }}>
                <Form action="/vk/saveFriendToCkeck" method="POST" onSubmit={this.onSubmit}>
                    <FormGroup controlId="notificationMail"
                               validationState={this.state.errors['notificationMail'] != null ? 'error' : null}>
                        <ControlLabel>{this.state.errors['notificationMail'] != null ? this.state.errors['notificationMail'] : 'Email'}</ControlLabel>
                        <FormControl type="email" name="notificationMail" required onInput={this.onAnyInput}/>
                        <HelpBlock>Email куда будут слаться уведомления</HelpBlock>
                    </FormGroup>
                    <FormGroup controlId="vkId"
                               validationState={this.state.errors['vkId'] != null ? 'error' : null}>
                        <ControlLabel>{this.state.errors['vkId'] != null ? this.state.errors['vkId'] : 'Пользователь'}</ControlLabel>
                        <FormControl type="text" name="vkId" required onInput={this.onAnyInput}/>
                        <HelpBlock>VK id или короткая ссылка пользователя за которым будем следить</HelpBlock>
                    </FormGroup>
                    <Checkbox validationState={null} name="sendChangeFriendsReport" checked="checked">
                        Отсылать отчет по изменении в друльях
                    </Checkbox>
                    <Checkbox validationState={null} name="sendOnlineReport" checked="checked">
                        Отсылать отчет о посещаемости
                    </Checkbox>
                    <ButtonToolbar>
                        <Button type="submit" bsStyle="primary">Сохранить</Button>
                        <Button type="reset" bsStyle="default">Сбросить</Button>
                    </ButtonToolbar>
                </Form>
            </div>
        );
    }
}