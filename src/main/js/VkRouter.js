/**
 * Created by ssr on 21.03.17.
 */
import {browserHistory, IndexRedirect, Router} from "react-router";
import React, {Component} from "react";
import {render} from "react-dom";
import VkSpyApp from "./VkSpyApp";
import Login from "./Login";
import VkChecker from "./VkChecker";
import VkSpyProfile from "./VkSpyProfile";


class Home extends Component {
    render() {
        //todo сделать home
        return (<div>home</div>);
    }
}

function onChange(source, destination) {
    let currentPath = destination.location.pathname + destination.location.search;
    window.parent.postMessage(currentPath, '*');
}

render(<Router history={browserHistory}>
    <Router path="/" component={VkSpyApp} onChange={onChange}>
        <IndexRedirect to='vk'/>
        <Router path="/login" component={Login} onChange={onChange}/>
        <Router path="vk" component={VkChecker} onChange={onChange}/>
        <Router path="/vk/profile" component={VkSpyProfile} onChange={onChange}/>
        <Router path="profile" component={VkSpyProfile} onChange={onChange}/>
    </Router>
</Router>, document.getElementById('root'));