/**
 * Created by ssr on 21.03.17.
 */
import React from "react";
import VkSpyHeader from "./VkSpyHeader";
import Grid from "react-bootstrap/lib/Grid";

export default class VkSpyApp extends React.Component {
    render() {
        return (<div>
            <VkSpyHeader/>
            <Grid>
                {this.props.children}
            </Grid>
        </div>)
    }
}