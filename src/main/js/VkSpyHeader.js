/**
 * Created by ssr on 21.03.17.
 */
import {Link} from "react-router";
import React from "react";
import Nav from "react-bootstrap/lib/Nav";
import Navbar from "react-bootstrap/lib/Navbar";
import NavItem from "react-bootstrap/lib/NavItem";

export default class Header extends React.Component {
    render() {
        return (
            <Navbar>
                <div className="container">
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link className="navbar-brand" to="/">Home</Link>
                        </Navbar.Brand>
                        {/*<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">*/}
                        {/*<span className="sr-only">Toggle navigation</span>*/}
                        {/*<span className="icon-bar"></span>*/}
                        {/*<span className="icon-bar"></span>*/}
                        {/*<span className="icon-bar"></span>*/}
                        {/*</button>*/}

                        {/*<a className="navbar-brand" href="#">Project name</a>*/}
                    </Navbar.Header>
                    <Nav>
                        {/*<ul className="nav navbar-nav">*/}
                            {/*<NavItem eventKey={1}><Link to="/login">Login</Link></NavItem>*/}
                            <NavItem eventKey={2}><Link to="/vk">VK</Link></NavItem>
                        {/*</ul>*/}
                    </Nav>
                </div>
            </Navbar>
        )
    }
}