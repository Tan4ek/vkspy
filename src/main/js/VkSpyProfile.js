/**
 * Created by ssr on 01.04.17.
 */
import React from "react";
import Axios from "axios";
import _ from "lodash";
import {Link} from "react-router";
import Form from "./Form";

// (function (axios) {
//     if (document.getElementById('_csrf_header') != null && document.getElementById('_csrf') != null) {
//         axios.defaults.headers.common[document.getElementById('_csrf_header').content] = document.getElementById('_csrf').content;
//     }
//     console.log('clouse work');
// }(Axios));

class UnsubscribeComplite extends React.Component {
    render() {
        return (<div><span>Вы успешно отписались!</span><br/><Link to="/vk">Главная</Link></div>)
    }
}

class VkSpyProfile extends React.Component {

    constructor() {
        super();
        this.state = {profileSaveForm: null};
        this.loadProfileInfo = this.loadProfileInfo.bind(this);
        this.onSaveProfile = this.onSaveProfile.bind(this);
        this.onUnsubscribe = this.onUnsubscribe.bind(this);
        this.handleProfileChange = this.handleProfileChange.bind(this);
        this.onLastSeen = this.onLastSeen.bind(this);
        this.loadProfileInfo();
    }

    loadProfileInfo() {
        const searchParams = new URLSearchParams(window.location.search);
        const profileSeachParam = searchParams.get('profile');
        this.setState({isExistProfile: !profileSeachParam});
        if (profileSeachParam) {
            Axios.get('/vk/profile/' + profileSeachParam).then((result) => {
                const resultObj = result.data;
                resultObj.profileEntity.profileId = resultObj.profileEntity.hash;

                const profileSaveForm = _.pick(resultObj.profileEntity, ['profileId', 'sendChangeFriendsReport', 'sendOnlineReport']);
                this.setState({
                    isExistProfile: true,
                    profileSaveForm: profileSaveForm,
                    user: resultObj.profileEntity.user,
                    profile: resultObj
                });
            }).catch((error) => {
                this.setState({error: error.response.data});
                //TODO samohvalov: notification
            })
        }
    }

    onUnsubscribe(event) {
        event.preventDefault();
        if (confirm('Вы уверены что хотите отписаться?')) {
            const form = event.currentTarget;
            Axios.post(form.action, {profile: this.state.profileSaveForm.profileId}).then((result) => {
                this.replaceState({unsubscribe: true});
            }).catch((error) => {
                this.setState({error: error.response.data});
            })
        }
    };

    onLastSeen(event) {
        event.preventDefault();
        const form = event.currentTarget;
        const inputDate = _.find(form.getElementsByTagName('input'), e => e.name === 'date');

        const _this = this;
        const changeState = (onLastSeenState) => {
            _this.setState({onLastSeen: onLastSeenState});
            setTimeout(() => _this.setState({onLastSeen: null}), 5000);
        };

        const searchParams = new URLSearchParams(window.location.search);
        const profileSeachParam = searchParams.get('profile');
        Axios.post('/vk/profile/' + profileSeachParam + '/sendLastSeen', {date: inputDate.value}).then((result) => {
            changeState('ok');
        }).catch((error) => {
            changeState('error');
        })
    }

    onSaveProfile(event) {
        event.preventDefault();
        const form = event.currentTarget;
        Axios.post(form.action, this.state.profileSaveForm).then((result) => {
            this.setState({saveProfile: true});
            const _this = this;
            setTimeout(() => _this.setState({saveProfile: false}), 5000);
        }).catch((error) => {
            this.setState({error: error.response.data});
        })
    }

    handleProfileChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        let profileSaveForm = this.state.profileSaveForm;

        this.setState({profileSaveForm: _.merge(profileSaveForm, {[name]: value})});
    }

    render() {
        //TODO samohvalov: диалог подтверждения на отписку
        const state = this.state;
        return (
            state.unsubscribe ? (<UnsubscribeComplite/>) :
                (state.profileSaveForm == null ?
                        (state.isExistProfile ?
                            <div>Загрузка</div> :
                            <div>
                                <label>Неверная ссылка </label>
                                <Link to="/vk">VK</Link>
                            </div> ) :
                        (
                            <div>
                                {state.error ? (
                                    <div><p className="text-warning">{state.error.error}</p></div>
                                ) : null}
                                {state.saveProfile ?
                                    (<p className="text-success">Профиль обновленн</p> ) : null}
                                <div className="form-horizontal">
                                    <div className="form-group">
                                        <p className="form-control-static">{state.profile.notificationMail}</p>
                                    </div>
                                    <div className="form-group">
                                        {state.user.photo50 ?
                                            <img src={state.user.photo50}
                                                 className="img-rounded"/> : null}
                                        <a href={'https://vk.com/id' + state.user.id}>{state.user.firstName + ' ' + state.user.lastName}</a>
                                    </div>
                                    <Form action="/vk/profile/save" method="POST" onSubmit={this.onSaveProfile}>
                                        <input name="profileId" type="hidden" required="required"
                                               value={state.profileSaveForm.profileId}
                                               onChange={this.handleProfileChange}/>
                                        <div className="form-group">
                                            <div className="checkbox">
                                                <label>
                                                    <input name="sendChangeFriendsReport" type="checkbox"
                                                           onChange={this.handleProfileChange}
                                                           checked={state.profileSaveForm.sendChangeFriendsReport}/>Отсылать
                                                    отчет
                                                    по
                                                    изменении в друльях
                                                </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="checkbox">
                                                <label>
                                                    <input name="sendOnlineReport" type="checkbox"
                                                           onChange={this.handleProfileChange}
                                                           checked={state.profileSaveForm.sendOnlineReport}/> Отсылать
                                                    отчет о
                                                    посещаемости
                                                </label>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-success">
                                                Сохранить
                                            </button>
                                        </div>
                                    </Form>
                                    <Form action='/vk/unsubscribe' method="POST" onSubmit={this.onUnsubscribe}>
                                        <input type="hidden" name="profile" value={state.profileSaveForm.profileId}/>
                                        <div className="form-group">
                                            <label className="control-label">Отписаться от всего</label><br/>
                                            <button type="submit" className="btn btn-danger">Отписаться</button>
                                        </div>
                                    </Form>
                                    <Form action={'/vk/profile/' + '/sendLastSeen'}
                                          method="POST" onSubmit={this.onLastSeen}>
                                        <div className="form-group">
                                            {state.onLastSeen === 'error' ?
                                                <lable className="control-label">Ошибка</lable> :
                                                (state.onLastSeen === 'ok' ?
                                                    <label className="control-label">Успешно отправленно на
                                                        email</label> : null) }
                                            <div className="checkbox">
                                                <input type="date" name="date"
                                                       defaultValue={new Date().toISOString().slice(0, 10)}/>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <label className="control-label">Получить посещение на этот день</label>
                                            <button type="submit" className="btn btn-success">Получить</button>
                                        </div>
                                    </Form>
                                    <Form
                                        action={'/vk/profile/' + new URLSearchParams(window.location.search).get('profile') + '/visitation/csv'}
                                        method="GET"
                                        target="_blank">
                                        <div className="form-group">
                                            <button type="submit" className="btn btn-success">
                                                Получить csv посещения
                                            </button>
                                        </div>
                                    </Form>
                                </div>

                            </div>
                        )
                )
        );
    }
}

export default VkSpyProfile;