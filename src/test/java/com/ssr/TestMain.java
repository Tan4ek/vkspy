package com.ssr;

import com.google.common.base.Predicate;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.ServiceClientCredentialsFlowResponse;
import com.vk.api.sdk.objects.friends.responses.GetResponse;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import org.junit.Ignore;
import org.junit.Test;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.time.Instant;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * Created by ssr on 27.10.16.
 */
public class TestMain {

    @Test
    public void test555() throws UnknownHostException, SocketException {
        Instant first = Instant.ofEpochMilli(1_000_000);
        Instant second = Instant.ofEpochMilli(2_000_000);
        Instant third = Instant.ofEpochMilli(3_000_000);

        System.out.println(first.compareTo(second));
        System.out.println(third.compareTo(second));

//        System.out.println(Date.from(Instant.now()));
//        InetAddress localHost = InetAddress.getLocalHost();
//        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
//        System.out.println(networkInterfaces);
//
//        System.out.println(localHost.toString());

    }

    @Test
    public void test() throws ClassNotFoundException {
        Reflections reflections = new Reflections("com.ssr");

        Reflections reflections1 = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forClassLoader())
                .setScanners(new SubTypesScanner(false)).filterInputsBy(new FilterBuilder().includePackage("com.ssr.beans")));

        for (String className : reflections1.getAllTypes()) {
            Class<?> aClass = Class.forName(className);
            Set<Method> get = ReflectionUtils.getAllMethods(aClass, new Predicate<Method>() {
                @Override
                public boolean apply(@Nullable Method method) {
                    return method.getName().startsWith("get");
                }
            });
            System.out.println("    " + get);
            System.out.println(aClass.getName());
        }
    }


    @Test
    @Ignore
    public void testvk() throws ClientException, ApiException {
        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vkApiClient = new VkApiClient(transportClient);

        int appId = 5910817;
        String sercretKey = "345MTpQLg1RgVElYh5LI";
        ServiceClientCredentialsFlowResponse authResponse = vkApiClient.oauth()
                .serviceClientCredentialsFlow(appId, sercretKey)
                .execute();

        ServiceActor actor = new ServiceActor(appId, sercretKey, authResponse.getAccessToken());
//        SearchResponse execute = vkApiClient.users().search(new UserActor(402105273, "9407c79d9407c79d945a7bb598945df6bc994079407c79dccba0ff3e82cc676b9018a96")).execute();

        List<UserXtrCounters> execute1 = vkApiClient.users().get(new UserActor(402105273, "9407c79d9407c79d945a7bb598945df6bc994079407c79dccba0ff3e82cc676b9018a96")).userIds("402105273").execute();

        GetResponse execute = vkApiClient.friends().get().userId(402105273).execute();
        // TODO: 07.03.17 execute содержит id добавившихся
        System.out.println(execute1);
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException, ClassNotFoundException {

        Reflections reflections = new Reflections("com.ssr");

        Reflections reflections1 = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage("com.ssr"))
                .setScanners(new SubTypesScanner(false)).filterInputsBy(new FilterBuilder().includePackage("com.ssr")));

        for (String className : reflections1.getAllTypes()) {
            Class<?> aClass = Class.forName(className);
            System.out.println(aClass.getName());
        }

//        for (Class<? extends Objects> aClass : reflections1.getSubTypesOf(Objects.class)) {
//            System.out.println(aClass.getName());
//        }
//
//        Set<Class<? extends Object>> allClasses =
//                reflections.getSubTypesOf(Object.class);
//        System.out.println("test");
//        for (Class<? extends Object> allClass : allClasses) {
//            System.out.println(allClass.getName());
//        }
//        ExecutorService executorService = Executors.newFixedThreadPool(3);
//
//        List<Future<String>> submits = new ArrayList<>();
//
//
//        submits.add(executorService.submit(new TestExecutor(1000)));
//        submits.add(executorService.submit(new TestExecutor(1002)));
//        submits.add(executorService.submit(new TestExecutor(100)));
//        submits.add(executorService.submit(new TestExecutor(2000)));
//        submits.add(executorService.submit(new TestExecutor(4000)));
//        submits.add(executorService.submit(new TestExecutor(100)));
//        submits.add(executorService.submit(new TestExecutor(150)));
//
//        boolean allDone = false;
//
//        while (!allDone) {
//            for (Iterator<Future<String>> iterator = submits.iterator(); iterator.hasNext(); ) {
//                Future<String> future = iterator.next();
//                if (future.isDone()) {
//                    System.out.println(future.get());
//                    iterator.remove();
//                }
//            }
//            if (submits.isEmpty()) {
//                allDone = true;
//            }
//        }
//        executorService.shutdown();
    }

    private static final class TestExecutor implements Callable<String> {

        private final int timeout;

        private TestExecutor(int timeout) {
            this.timeout = timeout;
        }

        @Override
        public String call() throws Exception {
            System.out.println("start thread: " + Thread.currentThread().getName());
            Thread.sleep(timeout);
            return "timeout is: " + timeout;
        }
    }
}
