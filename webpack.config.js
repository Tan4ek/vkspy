var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        vkRouter: './src/main/js/VkRouter',
    },
    devtool: 'source-map',
    cache: true,
    plugins: [
        new webpack.LoaderOptionsPlugin({
            debug: true
        }),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: true
        })
    ],
    output: {
        path: __dirname + '/src/main/resources/static/built',
        filename: '[name].js',
        library: '[name]'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
};